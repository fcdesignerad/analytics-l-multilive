<?php
// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Network\Email\Email;
use Cake\Core\Configure;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\Validation\Validation;
use Cake\I18n\I18n;
use Cake\Routing\Router;
use Cake\Controller\Component\CookieComponent;
use Cake\Datasource\ConnectionManager;

require_once(ROOT .  DS .'vendor' . DS . 'hybridauth' . DS . 'Hybrid' . DS . 'Auth.php');
use Hybrid;

require_once ROOT . '/vendor/autoload.php';

class UsersController extends AppController{    

    private $provider = "facebook";   
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        if($this->request->session()->read('language') != ""){
              I18n::locale($this->request->session()->read('language'));
        }
        
        // Allow users to register and logout. Testing file
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'register', 'delete', 'logout', 'forgotPassword', 'resetPassword', 'verifyEmail','multilikeVerifyEmail', 'edit', 'fblogin', 'fbindex', 'registerExtraInfo', 'saveExtraInfo', 'glogin', 'getConfig','notify', 'commonLogin','sendmautic','mauticapi','setcookieGDPR', 'editExpiredUsers', 'changePasswordExpiredUsers', 'deleteExpiredUsers']);
        $this->viewBuilder()->layout('admin_inner');
    }

	/**
	 * Portuguese
	 */
	public function analytics(){
		$this->set('language', $this->siteLanguage());
	}

	public function buildAnalyticsRequest($requestType, $startDate = "yesterday" , $endDate = "today", $nextFilter = ''){

		// Configure::write('debug', 2);

		$metrics = [];
		$filter = [];
		$analytics;

		$request = $this->buildAnalyticsRequestDefault($analytics, $metrics, $filter, $startDate, $endDate);

		switch( $requestType ){
			case "country":
				$this->createRequestBodyCountry($request);
				break;
			case "usersAndDevices":
				$this->createRequestBodyUsersAndDevices($request);
				break;
			case "states":
				$this->createRequestBodyStates($request);
				break;
			case "age":
				$this->createRequestBodyAge($request);
				break;
			case "gender":
				$this->createRequestBodyGender($request);
				break;
			case "usersPerDay":
				$this->createRequestBodyUsersPerDay($request, $metrics);
				break;
			case "exactCountry":
				$this->createRequestBodyExactCountry($request, $nextFilter, $filter);
				break;
			default:
				echo "default";
		}

		if( $requestType !== "usersPerDay"){
			$this->analyticsAddOrder($request);
		}

		$request->setMetrics($metrics);
		$request->setDimensionFilterClauses($filter);

		$body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
		$body->setReportRequests( [$request] );
		
		$output = $this->formatAnalyticsOutput( $analytics->reports->batchGet( $body ) );

		// debug($output);exit();

		if( $this->request->is('ajax') ){
			$response = $this->response->withType('json')->withStringBody( json_encode($output) );
			return $response;
		}
		else{
			return $output;
		}

	}

	private function buildAnalyticsRequestDefault(&$analytics, &$metrics, &$filter, $startDate = "yesterday" , $endDate = "today"){

		$VIEW_ID = "204709954";

		$analytics = $this->analyticsCreateClient();

		$request = new \Google_Service_AnalyticsReporting_ReportRequest();
		$request->setViewId($VIEW_ID);

		$this->analyticsAddDateRange($request, $startDate, $endDate);
		$this->analyticsAddMetric($metrics, "users");

		$form = TableRegistry::get("form_listings")->find('all')
			->select([
				'form_id',
				'created_by',
			])
			->where([
				'OR' => ['owner_id'=> $this->user('id'), 'created_by' => $this->user('parent_id') ] 
			])
			->first();

		if( !is_null($form) ){
			$this->analyticsAddFilter($filter, "pagePath", "/app/commingusers/view-form?id=".$form->form_id);
		}
		else if( ( $this->user('role') != 1 && $this->user('role') != 2 )  )
		{
			$this->Flash->success(__(error_403));
			return $this->redirect(['controller' => 'formbuilder', 'action' => 'listing']);
		}	

		return $request;
	}

	private function analyticsCreateClient(){

		$KEY_FILE_LOCATION = ROOT . '/config/api_multiplierapp_analytics.json';

		$client = new \Google_Client();
		$client->setApplicationName("Hello Analytics Reporting");
		$client->setAuthConfig($KEY_FILE_LOCATION);
		$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);

		return  new \Google_Service_AnalyticsReporting( $client );
	}

	private function analyticsAddDateRange(&$request, $startDate = "yesterday" , $endDate = "today"){

		// Create the DateRange object.
		$dateRange = new \Google_Service_AnalyticsReporting_DateRange();
		$dateRange->setStartDate($startDate);
		$dateRange->setEndDate($endDate);

		$request->setDateRanges($dateRange);
	}

	private function analyticsAddMetric(&$metrics, $metricName, $alias = Null){

		$metric = new \Google_Service_AnalyticsReporting_Metric();
		$metric->setExpression("ga:".$metricName);

		if($alias){
			$metric->setAlias($alias);
		}

		$metrics[] = $metric;
		
	}

	private function analyticsAddDimension(&$dimensions, $dimensionName){

		$dimension = new \Google_Service_AnalyticsReporting_Dimension();
		$dimension->setName("ga:".$dimensionName);

		$dimensions[] = $dimension;

	}

	private function analyticsAddSegment(&$segments, $segmentName, $field, $filterType, $filterValues){

		$dimensionFilter = new \Google_Service_AnalyticsReporting_SegmentDimensionFilter();
		$dimensionFilter->setDimensionName("ga:".$field);
		$dimensionFilter->setOperator($filterType);
		$dimensionFilter->setExpressions($filterValues);

		$segmentFilterClause = new \Google_Service_AnalyticsReporting_SegmentFilterClause();
		$segmentFilterClause->setDimensionFilter($dimensionFilter);

		$orFiltersForSegment = new \Google_Service_AnalyticsReporting_OrFiltersForSegment();
		$orFiltersForSegment->setSegmentFilterClauses(array($segmentFilterClause));

		$simpleSegment = new \Google_Service_AnalyticsReporting_SimpleSegment();
		$simpleSegment->setOrFiltersForSegment(array($orFiltersForSegment));

		$segmentFilter = new \Google_Service_AnalyticsReporting_SegmentFilter();
		$segmentFilter->setSimpleSegment($simpleSegment);

		$segmentDefinition = new \Google_Service_AnalyticsReporting_SegmentDefinition();
		$segmentDefinition->setSegmentFilters(array($segmentFilter));

		$dynamicSegment = new \Google_Service_AnalyticsReporting_DynamicSegment();
		$dynamicSegment->setSessionSegment($segmentDefinition);
		$dynamicSegment->setName($segmentName);

		$segment = new \Google_Service_AnalyticsReporting_Segment();
		$segment->setDynamicSegment($dynamicSegment);

		$segments[] = $segment;
	}

	private function analyticsAddOrder(&$request, $field = "users", $order = "DESCENDING"){

		$ordering = new \Google_Service_AnalyticsReporting_OrderBy();
		$ordering->setSortOrder($order);
		$ordering->setOrderType("VALUE");
		$ordering->setFieldName("ga:".$field);

		$request->setOrderBys($ordering);
	}

	private function analyticsAddFilter(&$filter, $filterDimensionName, $filterExpression, $operator = "EXACT"){

		$dimensionFilter = new \Google_Service_AnalyticsReporting_DimensionFilter();
		$dimensionFilter->setDimensionName("ga:".$filterDimensionName);
		$dimensionFilter->setOperator($operator);
		$dimensionFilter->setExpressions($filterExpression);	

		$dimensionFilterClause = new \Google_Service_AnalyticsReporting_DimensionFilterClause();
		$dimensionFilterClause->setFilters([$dimensionFilter]);

		$filter[] = $dimensionFilterClause;
	}

	/** Request Bodys */

	private function createRequestBodyCountry(&$request){

		$dimensions = [];

		$this->analyticsAddDimension($dimensions, "country");

		$request->setDimensions($dimensions);
	}

	private function createRequestBodyUsersAndDevices(&$request){

		$dimensions = [];
		$segments = [];

		$this->analyticsAddDimension($dimensions, "segment");
		$request->setDimensions($dimensions);

		$this->analyticsAddSegment($segments, 'new_users', 'userType', 'EXACT', ['New Visitor']);
		$this->analyticsAddSegment($segments, 'total_users', 'userType', 'IN_LIST', ['New Visitor', 'Returning Visitor']);
		
		$this->analyticsAddSegment($segments, 'mobile_users', 'deviceCategory', 'EXACT', ['mobile']);
		$this->analyticsAddSegment($segments, 'desktop_users', 'deviceCategory', 'EXACT', ['desktop']);

		$request->setSegments($segments);

	}

	private function createRequestBodyStates(&$request){

		$dimensions = [];

		$this->analyticsAddDimension($dimensions, "region");

		$request->setDimensions($dimensions);
	}

	private function createRequestBodyAge(&$request){
		$dimensions = [];

		$this->analyticsAddDimension($dimensions, "userAgeBracket");

		$request->setDimensions($dimensions);
	}

	private function createRequestBodyGender(&$request){
		$dimensions = [];

		$this->analyticsAddDimension($dimensions, "userGender");

		$request->setDimensions($dimensions);
	}

	private function createRequestBodyUsersPerDay(&$request, &$metrics){
	
		$dimensions = [];

		$this->analyticsAddDimension($dimensions, "date");

		$request->setDimensions($dimensions);
	}

	private function createRequestBodyExactCountry(&$request, $nextFilter, &$filter){

		$dimensions = [];

		$this->analyticsAddDimension($dimensions, "country");
		$request->setDimensions($dimensions);

		$this->analyticsAddFilter($filter, "country", $nextFilter);

	}

	/** Formating */

	private function formatAnalyticsOutput($output){

		$rows = $output[0]->getData()->getRows();

		$formated = [];
		$total = 0;

		foreach($rows as $row){

			$dimensions = $row->getDimensions();
			$metrics = $row->getMetrics();

			$formated[ $dimensions[0] ] = (int) $metrics[0]->getValues()[0];

			$total += (int) $metrics[0]->getValues()[0];
			
		}

		$formated['total'] = $total;
		
		return $formated;
	}


    /**
     * Get List usert in systems as per the users roles
     *
     * @return \Cake\Network\Response|null
     */
    public function index(){   
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        $this->set('roles', $roles);
        $query = $this->Users->find('all', ['contain' => ['Groups']]); 
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
		    $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }	   	
        if( $userRole == 3 ){
            $query->where(['parent_id' => $this->Auth->user('id')]);
        }else if( $userRole == 1 ){
            $query->where(['role NOT IN' => array('1') ]);
        }elseif( $userRole == 5){
    			if($userId==$userSegmentId){
                   $query->where(['role NOT IN' => array('1', '2','5'),'segment_id' =>$userSegment]);
    			}else{
    			   $query->where(['parent_id' => $this->Auth->user('id')]);
    			}	
        }else{
            $query->where(['role NOT IN' => array('1', '2') ]);
        }

        $query = clean($query);
        
        $groupsModel = TableRegistry::get("groups");
        $applicationModel = TableRegistry::get("applications");

        foreach ($query as &$user) {
            if ($user->role == 2 OR $user->role == 4){
                $groupData  = $groupsModel->find('all')->where(['owner_id' => $user->parent_id])->first();
                $user->group = $groupData;
            }            

          if ($user->role == 4){
               $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$user->parent_id])->first();
             $user->application = $applications;
             }else{  
        $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$user->id])->first();
        $user->application = $applications;            
        }
        }

        //////////get active users for admin user list///
        $userPlansModel = TableRegistry::get("user_plans");
        if( $userRole == 1 || $userRole == 2){
          $activeUsers = array();
          foreach ($query as &$user) {
            if($user->role == 2){
              array_push($activeUsers,$user);
            }
            else if($user->role == 3 && $user->parent_id == 0){
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$user->id, 'is_expired'=>0])->first();
                if(count($userPlansData) == 1){
                  array_push($activeUsers,$user);
                }
            }else if($user->role == 4 && $user->parent_id != 0){
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$user->parent_id, 'is_expired'=>0])->first();
              if(count($userPlansData) == 1){
                  array_push($activeUsers,$user);
              }
            }else if($user->role == 5){
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$user->id, 'is_expired'=>0])->first();
              if(count($userPlansData) == 1){
                  array_push($activeUsers,$user);
              }
            }else{

            }
            
          }
        }
        //////////get active users for admin user list///

        $usersModel = TableRegistry::get("Users");
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();

        if( $userRole == 3 || $userRole == 4 || $userRole == 5){
              
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$userId, 'is_expired'=>0])->first();
              if(count($userPlansData) == 0){
                $this->set("isExpired", 1);
                $this->set("user_type", $user_details->registerFrom);
              }else{
                $this->set("isExpired", 0);
                $this->set("user_type", $user_details->registerFrom);
              }  

              
        }else{
          $this->set("isExpired", 0);
          $this->set("user_type", $user_details->registerFrom);
        }

		  $segmentModel = TableRegistry::get("segments");
	    $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray();

      $FormListingModel = TableRegistry::get("form_listings");  
    $FormSingleData=$FormListingModel->find('list', [
        'keyField' => 'owner_id',
        'valueField' => 'isUnderServer'
        ])->toArray();


        $this->set('segments', $segments);        
        $this->set('FormSingleData', $FormSingleData);    
        if( $userRole == 1 || $userRole == 2){     
          $this->set('users', $activeUsers);
        }else{
        $this->set('users', $query);
        }
        $this->set('page', 'users');
        $this->set('_serialize', ['Users']);
         if($this->Auth->user('segment_id') != 0){
            $segmentModel = TableRegistry::get("segments");
            $segmentsMain  = $segmentModel->find('all')->where(['id' => $this->Auth->user('segment_id')])->first();
           if(!empty($segmentsMain) && ($segmentsMain->name != "fe" || $segmentsMain->name != "Fé")){
              if(!$this->allowRestrictedContent()){
                  $this->Flash->error(__(error_no_plan));
              }
          }
        }else{
          if(!$this->allowRestrictedContent()){
              $this->Flash->error(__(error_no_plan));
          }
        }
    }

    function fbindex(){
      if ($_GET['error'] =='access_denied')  // if user cancelled facebook login
      {
        $this->Flash->error(__($_GET['error_description']));
        return $this->redirect(array("controller" => "users", "action" => "login"));
      }
      require_once(ROOT .  DS .'vendor' . DS . 'hybridauth' . DS . 'Hybrid' . DS . 'Auth.php');
      require_once(ROOT .  DS .'vendor' . DS . 'hybridauth' . DS . 'Hybrid' . DS . 'Endpoint.php');
      \Hybrid_Endpoint::process();
    }

    private function getConfig(){
      return array(
        "base_url" => HTTP_ROOT."users/fbindex/",
        "providers" => array(
            "Google" => array(
                "enabled" => true,
                "keys" => array("id" => GMAIL_APP_ID, "secret" => GMAIL_APP_SECRET),
                //"approval_prompt" =>'force',
                'access_type' =>"offline",
                "scope" => " https://www.googleapis.com/auth/youtube.readonly https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/plus.login " . // optional
                "https://www.googleapis.com/auth/plus.me " . // optional
                "https://www.googleapis.com/auth/plus.profile.emails.read", // optional
            ),
            "Facebook" => array ( 
              "enabled" => true,
              "keys"    => array("id" => FB_APP_ID, "secret" => FB_APP_SECRET, "trustForwarded" => true, "allowSignedRequest" => false), 
              'scope'   => 'email, public_profile, user_events, business_management, manage_pages, publish_pages, publish_video, publish_to_groups, read_insights, user_friends', 
              'trustForwarded' => false
            ),
        ),

        // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
        "debug_mode" => false,
        "debug_file" => ""
      );
    } 

    public function glogin() {
      $this->autoRender = false; 
      $this->provider = "google";  
      $this->commonLogin();      
    }

    public function fblogin() {
      $this->autoRender = false; 
      $this->provider = "facebook";
      $this->commonLogin();      
    }

    public function commonLogin(){
      $this->autoRender = false; 
      try {
        $hybridauth = new \Hybrid_Auth($this->getConfig());
        $authProvider = $hybridauth->authenticate($provider = $this->provider);
        $user_profile = $authProvider->getUserProfile();
        if ($user_profile && isset($user_profile->identifier)) {
          if($provider == "google"){
            $socialInfo = array("socialId"=> $user_profile->identifier, "name"=> $user_profile->displayName, "email"=> $user_profile->email, "loginType"=> $this->provider);
          }else{
            $access_token = $user_profile->accessToken;
            $socialInfo = array("socialId"=> $user_profile->identifier, "firstName"=> $user_profile->firstName, "lastName"=> $user_profile->lastName, "email"=> $user_profile->email, "phone"=> $user_profile->phone, "loginType"=> "facebook", 'access_token'=> $access_token);
          }
          $this->checkUserLogin($socialInfo);
        }
      } catch (\Exception $e) {
        switch ($e->getCode()) {
          case 0 : echo "Unspecified error.";
              break;
          case 1 : echo "Hybridauth configuration error.";
              break;
          case 2 : echo "Provider not properly configured.";
              break;
          case 3 : echo "Unknown or disabled provider.";
              break;
          case 4 : echo "Missing provider application credentials.";
              break;
          case 5 : echo "Authentication failed. "
              . "The user has canceled the authentication or the provider refused the connection.";
              break;
          case 6 : echo "User profile request failed. Most likely the user is not connected "
              . "to the provider and he should to authenticate again.";
              $this->logout();
              break;
          case 7 : echo "User not connected to the provider.";
              $this->logout();
              break;
          case 8 : echo "Provider does not support this feature.";
                break;
        }
        // well, basically your should not display this to the end user, just give him a hint and move on..
        echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();

        echo "<hr /><h3>Trace</h3> <pre>" . $e->getTraceAsString() . "</pre>";
      }
    }   

    public function checkUserLogin($info){
        $this->autoRender = false; // avoid to render view
        $UsersModel = TableRegistry::get("users");
        $user_details = $UsersModel->find('all')->where(['email' => $info['email']])->first();
        if(!empty($user_details)){
            $UserData = $UsersModel->newEntity();
            $UserData->id = $user_details['id'];
            if($info['loginType'] == 'facebook'){
              $UserData->fb_id = $info['socialId'];
              if(!empty($info['firstName']) && !empty($info['lastName'])){
                $UserData->name = $info['firstName'] .' '. $info['lastName'];
              }
            }else if($info['loginType'] == 'google'){
              $UserData->g_id = $info['socialId'];
              if(!empty($info['name'])){
                $UserData->name = $info['name'];
              }
            }

            if(!empty($info['phone'])){
              $UserData->phone = $info['phone'];
            }

            if($UsersModel->save($UserData)){
                $session = $this->request->session();
                $session->write('loginType', $info['loginType']);
                if(!empty($info['access_token'])){
                  $session->write('access_token', $info['access_token']);
                }
                $this->Auth->setUser($user_details->toArray());
                return $this->redirect($this->Auth->redirectUrl());
            }else{
                $this->Flash->error(__(login_failed));
                return $this->redirect(array("controller" => "users", "action" => "login"));
            }
        }else{
            $session = $this->request->session();
            $session->write('socialInfo', json_encode($info)); 
            return $this->redirect(array("controller" => "users", "action" => "registerExtraInfo"));
        }
    }

    public function registerExtraInfo(){
        $session = $this->request->session();
        $this->viewBuilder()->layout('admin_login');
        if(!empty($session->read('socialInfo'))){
            $socialInfo = $session->read('socialInfo');
            $this->set('socialInfo', $socialInfo);
            $session->delete('socialInfo'); 

            $segmentModel = TableRegistry::get("segments");
            $segments  = $segmentModel->find('list', [
                'keyField' => 'id',
                'valueField' => 'name'
            ])->where(['status' => 1])->toArray();      
            $this->set('segments', $segments);
        }else{
          $this->logout();
          return $this->redirect(array("controller" => "users", "action" => "login"));
        }
    }

    public function saveExtraInfo(){
        $this->autoRender = false; // avoid to render view
        if ($this->request->is('post')) {
            if(!empty($this->request->data['username']) && !empty($this->request->data['password']) && !empty($this->request->data['phone']) && !empty($this->request->data['socialInfo']) && !empty($this->request->data['segment_id'])){

                $socialData = json_decode($this->request->data['socialInfo'], True);
                
                $register_url = HTTP_ROOT. 'api/register';

                $Postdata = array(
                    "email" => $socialData['email'],
                    "username" => $this->request->data['username'],
                    "password" => $this->request->data['password'],
                    "phone" => $this->request->data['phone'],
                    "segment_id" => $this->request->data['segment_id'],
                    "role" => 3,
                    "language" => "Portuguese",
                    "register_from" => "WebApp",
                    "webservice" => 1,
                    "login_type" => $socialData['loginType'],
                    "social_id" => $socialData['socialId']
                );

                if($socialData['loginType'] == 'facebook'){
                  $Postdata['fb_id'] = $socialData['socialId'];
                  if(!empty($socialData['firstName']) && !empty($socialData['lastName'])){
                    $Postdata['name'] = $socialData['firstName'] .' '. $socialData['lastName'];
                  }
                }else if($socialData['loginType'] == 'google'){
                  $Postdata['g_id'] = $socialData['socialId'];
                  if(!empty($socialData['name'])){
                    $Postdata['name'] = $socialData['name'];
                  }
                }

                $headers = array(
                  'Content-Type:application/json'
                );

                $result = $this->postCurl($register_url, json_encode($Postdata), $headers);
                $result = json_decode( $result );
                //echo '<pre>'; print_r($result); die;
                if($result->status == 1){
                    $configData=Configure::read('sonoconfig.mautic');
                    $this->updateMauticData($configData['account_subscriber_segment_id']);
                    if (Validation::email($Postdata['username'])) {
                        $this->Auth->config('authenticate', [
                            'Form' => [
                                'fields' => ['username' => 'email']
                            ]
                        ]);
                        $this->Auth->constructAuthenticate();
                        $Postdata['email'] = $Postdata['username'];
                        unset($Postdata['username']);
                    }
                    
                    $user = $this->Auth->identify();
                    if ($user) {
                      $session = $this->request->session();
                      $session->write('loginType', $socialData['loginType']);
                      if(!empty($socialData['access_token'])){
                        $session->write('access_token', $socialData['access_token']);
                      }
                        $this->Auth->setUser($user);
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                    $this->Flash->error(__(invalid_password));
                }else{
                    $this->Flash->error(__(registration_failed));
                    return $this->redirect(array("controller" => "users", "action" => "login"));
                }
            }else{
                $this->Flash->error(__(invalid_parameters));
                return $this->redirect(array("controller" => "users", "action" => "login"));
            }
        }
    }
    
    public function logout(){
      // Social logout  
      $session = $this->request->session();
      if(!empty($session->read('loginType'))){
      $hybridauth = new \Hybrid_Auth( $this->getConfig());
        $authProvider = $hybridauth->authenticate($provider = $session->read('loginType'));
      $user_profile = $authProvider->logout();  
        if($session->read('loginType') == "google"){
      $hybridauth->logoutAllProviders();
        }     
      }
      

      // Webapp logout
	  $this->request->session()->delete('AdminUsers');
	  $this->request->session()->delete('segmentAdminUsers');
      $this->Auth->logout();
      $session->delete('loginType');
      $session->delete('access_token');
      
      return $this->redirect(['action' => 'login']);
    }

    public function login(){
        $this->viewBuilder()->layout('admin_login');
        if ($this->request->is('post')) {
            if (Validation::email($this->request->data['username'])) {
                $this->Auth->config('authenticate', [
                    'Form' => [
                        'fields' => ['username' => 'email']
                    ]
                ]);
                $this->Auth->constructAuthenticate();
                $this->request->data['email'] = $this->request->data['username'];
                unset($this->request->data['username']);
            }
            
            $user = $this->Auth->identify();
            
            if ($user) {
              if($user['is_pipefy_verified']!=0){
              $this->lastLogin($user['id']);
                $this->Auth->setUser($user);
                $form_type=$this->request->data['form_type'];
                $multilike_id=$this->request->data['multilike_id'];
                if($form_type=='multilike'){
                  $multilikehaschildmodel=TableRegistry::get("tbl_multilike_user_has_child");
                  $ismultilikechildfound =$multilikehaschildmodel->find('all')->where(['form_id'=>$multilike_id,'user_id'=>$user['id']])->first();
                  if(empty($ismultilikechildfound)){
                    $multilikehaschildata = $multilikehaschildmodel->newEntity();
                    $multilikehaschildata->form_id=$multilike_id;
                    $multilikehaschildata->user_id=$user['id'];
                    $multilikehaschildmodel->save($multilikehaschildata);
                  }
                  return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-user-view-socialnetwork?id='.$user['id']]);
                }else{
                return $this->redirect($this->Auth->redirectUrl());
            }
            }else{
            $this->Flash->error(__(pipefy_registration_msg));
            }
            }else{
            $this->Flash->error(__(invalid_password));
            }
            
            $form_type=$this->request->data['form_type'];
            if($form_type=='multilike'){
              $multilike_id=$this->request->data['multilike_id'];
              return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-form-login?id='.$multilike_id]);
            }
        }
    }

    public function register(){
        $this->viewBuilder()->layout('admin_login');
		    $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name',
        'order' => ['name' => 'ASC']
        ])->where(['status' => 1])->toArray(); 		
        $countries=$this->getCountryCodesWithDialCode();
        $this->set('countries', $countries);
		    $this->set('segments', $segments);
        if ($this->request->is('post')) {
            $register_url = HTTP_ROOT. 'api/pipefyRegister';
            $data = array(
                'username' => $this->request->data['username'], 
                'email'=> $this->request->data['email'], 
                'password'=> $this->request->data['password'], 
                'first_name'=> $this->request->data['first_name'], 
                'last_name'=> $this->request->data['last_name'], 
                'phone'=> $this->request->data['phone'], 
                'segment_id'=> $this->request->data['segment_id'], 
                'language'=> 'Portuguese', 
                'role'=> '3', 
                'webservice'=> '1',
                'register_from'=> 'WebApp',
                'ref_medium' => $this->request->data['ref_medium']

            );
            $headers = array(
                'Content-Type:application/json'
            );
            $result = $this->postCurl($register_url, json_encode($data), $headers);
            $result = json_decode($result);
            if( $result->status == 1 ){
                $configData=Configure::read('sonoconfig.mautic');
                $this->updateMauticData($configData['account_subscriber_segment_id']);
                if (Validation::email($this->request->data['username'])) {
                    $this->Auth->config('authenticate', [
                        'Form' => [
                            'fields' => ['username' => 'email']
                        ]
                    ]);
                    $this->Auth->constructAuthenticate();
                    $this->request->data['email'] = $this->request->data['username'];
                    unset($this->request->data['username']);
                }
                
                $user = $this->Auth->identify();
                if ($user) {
                    $this->lastLogin($user['id']);
                    $this->Auth->setUser($user);
                    /*
                    custom flash message
                    */
                    $this->Flash->registersuccess(__(you_are_successfully_registered));
                    return$this->redirect($this->referer());


                }
                $this->Flash->error(__(invalid_password));
            }else{
                $this->Flash->error(__(registration_failed));
            }        
        }
    }
   
   /**
    Function for forgot password
   */
    function forgotPassword(){
       
        $this->viewBuilder()->layout('admin_login');
        // Loaded Admin Model
        $usersModel = TableRegistry::get('Users');
        $userData = $usersModel->newEntity();
        
        if($this->request->is('post')==1){
            if(isset($this->request->data['email']) && $this->request->data['email']!=''){
                $user = $usersModel->find('all',['conditions' => ['Users.email' => $this->request->data['email']]]);
                $getAdminData =  $user->first();
                
                if(empty($getAdminData)){
                    $this->Flash->error(__(no_email));
                   
                }else{
                    date_default_timezone_set("UTC");
                    $dateNow = date('Y-m-d H:i:s');

                    $new_pass_key  = md5(rand().microtime());

                    $userData->id = $getAdminData->id;
                    $userData->new_pass_key = $new_pass_key;
                    $userData->new_password_requested = $dateNow;
                    //save adnin data       
                    if($usersModel->save($userData)){
                        //AddedGourav
                        $oldLang = $this->changeLanguageForEmail($getAdminData->id);
                        $adminId = base64_encode(convert_uuencode($getAdminData->email));


                        $replace = array('{user}','{link}');

                        $link = HTTP_ROOT.'users/reset-password/'.$adminId.'/'.$new_pass_key;
                        $linkOnMail = '<a href="'.$link.'" target="_blank">'.__(Click_here_to_reset_password).' </a>';

                        $with = array($getAdminData->username, $linkOnMail);
                        //Send email function
                        // $this->send_email('',$replace,$with,'admin_forgot_password',$getAdminData->email);

                        $data = array( 'email' => $getAdminData->email, 'subject' => __(ResetPassword), 'email_heading' =>__(ResetPassword),'name' => $getAdminData->username, 'link' => $linkOnMail );
                        $response = $this->send_email_html($data,'admin_forgot_password');        
                        $this->changeLanguageAfterEmail($oldLang);
                        $this->Flash->success(__(pwd_sent));
                    }
                }
            }else{
                $this->Flash->error(__(no_email_validation));
                
            }
        }
    }

    //function to verify user email
    function multilikeVerifyEmail($uid = null){
      $UsersModel = TableRegistry::get('tbl_social_network_user');
      $UserData = $UsersModel->newEntity();
      $userId = base64_decode($uid);
      $user = $UsersModel->find('all')->where(['id'=> $userId])->first();
      if(!empty($user)){
        $UserData->is_email_verified = true;
        $UserData->id = $userId;
        $UsersModel->save($UserData);
        $session->write('successfull',__(email_varification_succ));
        return $this->redirect(['controller' => 'Commingusers', 'action' => 'multilike-view-form?id='.$user->tbl_multilike_form_id]);
      }else{
        echo __(USER_NOT_EXISTS);die;
      }
    }

     //function to verify user email
     function verifyEmail($uid = null){
       $UsersModel = TableRegistry::get('Users');
       $UserData = $UsersModel->newEntity();
       $userId = base64_decode($uid);
       $user = $UsersModel->find('all')->where(['id'=> $userId])->first();
       if(!empty($user)){
         $UserData->is_email_verified = true;
         $UserData->id = $userId;
         $UsersModel->save($UserData);
         $this->Flash->success(__(email_varification_succ));
         return $this->redirect(['controller' => 'Users', 'action' => 'login']);
       }else{
         $this->Flash->error(__(USER_NOT_EXISTS));
         return $this->redirect(['controller' => 'Users', 'action' => 'login']);
       }
     }
    /**
     * Replace user password (forgotten) with a new one (set by user).
     * User is verified by user_id and authentication code in the URL.
     * Can be called by clicking on link in mail.
     *
     * @return void
     */
    function resetPassword($uid = null, $key= null)
    {
        $this->viewBuilder()->layout('admin_login');
      
        $session = $this->request->session();
        // Loaded Admin Model
        $UsersModel = TableRegistry::get('Users');
        $UserData = $UsersModel->newEntity();

        $this->request->data = @$_REQUEST;
        $uid = convert_uudecode(base64_decode($uid));

        if($uid !=""){
            $this->set("email",$uid);
        }else{
            $uid = $this->request->data['Users']['email'];
            $this->set("email",$uid);
        }
        
        if($key !=""){
            $this->set("key",$key);
        }else{
            $key = $this->request->data['Users']['key'];
            $this->set("key",$key);
        }
        
        $count = $UsersModel->find("all",["conditions"=>['Users.email'=>$uid,'Users.new_pass_key'=>$key]])->first();
        
        if(!empty($count)){
             date_default_timezone_set("UTC");
             $dateNow = date('Y-m-d H:i:s');

             $diff_in_minute = $this->get_date_diff($count->new_password_requested ,$dateNow);
             
             $diff_in_hr = $diff_in_minute/60;
             if($diff_in_hr >= 24){
                 $this->Flash->error(__(link_expire));

                 return $this->redirect(['controller' => 'Users', 'action' => 'login']);
             }
        }
        if(isset($count->email) &&  $count->new_pass_key==$key)
        { 
            

            if(isset($this->request->data['Users']) && !empty($this->request->data['Users'])){
            
                $data = $this->request->data;
                $error=$this->validate_resetPwd($data);
               

                if(count($error) == 0)
                {
                 
                    $UserData->password = $this->request->data['Users']['password'];//(new DefaultPasswordHasher)->hash($this->request->data['Users']['password']);
                    $UserData->new_password_requested = null;
                    $UserData->new_pass_key = null;
                    $UserData->id = $count->id;

                    

                    $UsersModel->save($UserData);
                    $getUserData =  $UsersModel->find('all',
                                            ['conditions' => ['Users.email' => $uid]]
                                        )->toArray();
                    
                       

                    $replace = array('{full_name}');
                    $with = array($count->name);
                    // $this->send_email('',$replace,$with,'reset_password',$count->email);
                    $oldLang = $this->changeLanguageForEmail($count->id);
                    $data = array( 
                        'email' => $count->email, 
                        'full_name' => $count->name, 
                        'subject' => __(Password_changed),
                        'email_heading' =>  __(Password_changed)
                        );
                    $response = $this->send_email_html($data,'reset_password');    
                    $this->changeLanguageAfterEmail($oldLang);    

                    $this->Flash->success(__(pws_reset_succ));

                     return $this->redirect(['controller' => 'Users', 'action' => 'login']); 
                }else{
                
                    $this->set('resetError',$error);
                    foreach($error['re_password'] as $singleError){
                        $this->Flash->error(__($singleError));
                    }
                     
                   $uid = base64_encode( convert_uuencode($uid));
                    return $this->redirect('/Users/reset-password/'.$uid.'/'.$key);
                    
                }
            }   
        }else{

             
                 $this->Flash->error(__(auth_fail));
             
           return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    
        
    }
    function get_date_diff($date1, $date2)
    {
        
          $date1 = strtotime($date1);
          $date2 = strtotime($date2);

         $old_date = $date1;
         $new_date = $date2;
        
        $datediff = $new_date-$old_date; //strtotime($date2) - strtotime($date1);
        $get_minutes = ceil($datediff/60);
        
    return $get_minutes;
    } 
/**
    * Function for Validate RESET PASSWORD
    */
    function validate_resetPwd($data)
    {
        
        $errors=array();
                
        if(trim($data['Users']['password'])=="")
        {
            $errors['password'][]= __(pwd_val_error);
        }
        else 
        {
            $length=strlen($data['Users']['password']);
            if($length < 6)
            {
                $errors['password'][]= __(min_letter);
            }
        }
        if(trim($data['Users']['re_password'])=="")
        {
            $errors['re_password'][]= __(confirm_password);
        }
        else 
        {
            if($data['Users']['password'] != $data['Users']['re_password'])
            {
                $errors['re_password'][] = __(pwd_not_match);
            }
        }   
        return $errors;
    }
 
  
    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    public function add()
    {   
        $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray(); 		
		    $this->set('segments', $segments);
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->status = 1;
            $user->parent_id = $this->Auth->user('id');
            if ($this->Users->save($user)) {
                $this->Flash->success(__(user_saved));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__(user_add));
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        
        $userRole = $this->Auth->user('role');
        switch ( $userRole ) {
            case '1':
                unset($roles['1']);
                unset($roles['5']);
                break;
            case '2':
                unset($roles['1']);
                unset($roles['2']);
				        unset($roles['5']);
                break;
            case '3':
                unset($roles['1']);
                unset($roles['2']);
                unset($roles['3']);
                unset($roles['5']);
                break;
            default:
                break;
        }       

        $usersModel = TableRegistry::get("Users");
        $user_details = $usersModel->find('all')->where(['id' => $this->Auth->user('id')])->first();

        if( $userRole == 3 || $userRole == 4 || $userRole == 5){
              $userPlansModel = TableRegistry::get("user_plans");
              $userPlansData = $userPlansModel->find('all')->where(['user_id' => $this->Auth->user('id'), 'is_expired'=>0])->first();
              if(count($userPlansData) == 0){
                $this->set("isExpired", 1);
                $this->set("user_type", $user_details->registerFrom);
              }else{
                $this->set("isExpired", 0);
                $this->set("user_type", $user_details->registerFrom);
              }  

              
        }else{
          $this->set("isExpired", 0);
          $this->set("user_type", $user_details->registerFrom);
        }
               
        $this->set('roles', $roles);
        //pr($user->errors());die;
        $this->set('user', $user);
        $this->set('page', 'users_add');
        if($this->Auth->user('segment_id') != 0){
            $segmentModel = TableRegistry::get("segments");
            $segmentsMain  = $segmentModel->find('all')->where(['id' => $this->Auth->user('segment_id')])->first();
          if(!empty($segmentsMain) && ($segmentsMain->name != "fe" || $segmentsMain->name != "Fé")){
              if(!$this->allowRestrictedContent()){
                  $this->Flash->error(__(error_no_plan));
              }
          }
        }else{
            if(!$this->allowRestrictedContent()){
                $this->Flash->error(__(error_no_plan));
            }
        }
    }

    /**
     * Edit Users
     *
     * @param string|null $id Application id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {   
        
		$segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray(); 		
		$this->set('segments', $segments);
        $segmentDetail = $segmentModel->find('all')->where(['segment_admin' =>$id])->first();
        $user = $this->Users->get( $id, ['contain' => [] ] );
        if ($this->request->is(['patch', 'post', 'put'])) {
			$role = $this->request->data['role'];
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
				if(!empty($segmentDetail) && $role!=5){
					unset($this->request->data['name']);
					$this->request->data['segment_admin']=0;
					$segmentData = $segmentModel->newEntity();
					$segmentData = $segmentModel->patchEntity($segmentData, $this->request->data);
					$segmentData->id = $segmentDetail->id;
					$segmentModel->save($segmentData);
				}	
                $this->Flash->success(__(saved));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__(savenot));
            }
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        unset($roles['1']);

        if($this->Auth->user('role') == 3){
           unset($roles['2']);
           unset($roles['3']);
        }

		if($user->role != 5){
          unset($roles['5']);
		}
        unset($user->password);
        $this->set('roles', $roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }




    /**
     * Change Password
     *
     * @param string|null $id Application id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function changePassword($id = null)
    {   
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__(Passwordupdated));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__(savenot));
            }
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        unset($roles['1']);
        unset($user->password);
        $this->set('roles', $roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
    }


    public function profilePassword($id = null){   
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) 
            {
                $this->Flash->success(__(profile_saved));
                return $this->redirect(['action' => 'profilePassword/'.$id]);
            } else {
                $this->Flash->error(__(pws_not_saved));
                return $this->redirect(['action' => 'profilePassword/'.$id]);
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
    }

    public function updateProfile($id = null){   
        if($id != $this->Auth->user('id')){
            $this->Flash->error(__(notaut));
               return $this->redirect(['action' => 'index']);
        }
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $FormListingModel = TableRegistry::get("form_listings");
            $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$id])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if(isset($FormSingleData) && !empty($FormSingleData)){
               $FormSingleData->language = $this->request->data('language');
             $FormListingModel->save($FormSingleData); 
            }


            if ($this->Users->save($user)) 
            {
                $this->Flash->success(__('Passwordupdated'));
                return $this->redirect(['action' => 'updateProfile/'.$id]);
            } else {
                $this->Flash->error(__(profile_not_saved));
                return $this->redirect(['action' => 'updateProfile/'.$id]);
            }
        }
        $this->set('language', $FormSingleData->language);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
    }




    /**Function for dashboard
    */
    function dashboard(){      
        /*$AdminsModel = TableRegistry::get('Users');
        $admindata = $AdminsModel->find('all');
        $AllAdmins = $admindata->all()->first(); 
        $this->set('admins_info',$AllAdmins);*/
        $roleId = $this->Auth->user('role');
        $userId =  $this->Auth->user('id'); 
    		$userSegment = $this->Auth->user('segment_id');
    		$usersModel = TableRegistry::get("Users");
  	    $usersList  = $usersModel->find('list', [
          'keyField' => 'id',
          'valueField' => 'id'
          ])->where(['segment_id' =>$userSegment,'role !=' =>5])->toArray();
    		$usersList[$userId] = $this->Auth->user('id');
    		$SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
    		$userSegmentId=0;
    		if(!empty($SegmentData)){
    		  $userSegmentId = $SegmentData->segment_admin;
    		}
        $form_id = 0;
        /*$liveBroadCastDetailsModel = TableRegistry::get('live_broadcasts');
        $query = $liveBroadCastDetailsModel->find(); 
        $query
        ->select([
        'followers' => $query->func()->sum('followers'),
        ]);*/
        if($roleId == 3 || $roleId == 4 ){
            if($roleId == 4 ){
                $userId = $this->Auth->user('parent_id');               
            }
            $FormListingModel = TableRegistry::get("form_listings");
            $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
            $form_id = $FormSingleData->form_id;
      			if(!empty($FormSingleData)){
                   //$reached = $query->where(['form_id' => $form_id])->first();
      			}
        }elseif($roleId == 5){
            $FormListingModel = TableRegistry::get("form_listings");
			      if($userId==$userSegmentId && !empty($usersList)){
                $FormSingleData=$FormListingModel->find('all')->where(['owner_id IN'=> $usersList])->first();
                $form_id = $FormSingleData->form_id;
            }else{
      				$FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
      				$form_id = $FormSingleData->form_id;
    		    }	
            //$reached = $query->where(['form_id' => $form_id])->first();
        }else{
            //$reached = $query->first();
        }
        $this->set('form_id',$form_id);
        //$this->set('reached',$reached->followers);
        $DashboardVideosModel = TableRegistry::get('dashboard_videos');
        $videos = $DashboardVideosModel->find('all')->toArray();
        $this->set('videos',$videos);
        $this->set('page','dashboard');
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }



    /**
     * Delete method
     *
     * @param string|null $id Application id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null){
        $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        if(isset($this->request->data)&& !empty($this->request->data)){   
            if ($user){
                // Get application details
                $AppModel = TableRegistry::get('applications');
                //$appDetail = $AppModel->find('all')->where(['manager_id' => $id])->first();
                $appDetail = $AppModel->find('all')->where(['manager_id' => $id])->toArray();
                if(count($appDetail) > 0){
                  $group_id = $appDetail[0]['group_id'];

                  foreach($appDetail as $app){
                    $this->removeWowzaApp($app['application_name']);                           
                  }
                  
                  $CommingUserModel = TableRegistry::get('comming_users');

                  /*$StreamData = $CommingUserModel->find('all')->where(['group_id' => $appDetail->group_id])->toArray();
                  if(count($StreamData) > 0){
                      foreach($StreamData as $key => $value){
                          // Delete app from wowza
                          $this->removeWowzaApp($value['app_name']);                           
                      }
                  }else{
                      $this->removeWowzaApp($appDetail->application_name);                        
                  }*/                   


                  // Delete registered social users
                  $CommingUserModel->deleteAll(['group_id' => $group_id],false);

                  // Delete broadcasts
                  $LiveBroadcastModel = TableRegistry::get('live_broadcasts');
                  $LiveBroadcastModel->deleteAll(['group_id' => $group_id],false);

                  // Delete streams
                  $LiveStreamModel = TableRegistry::get('live_streams');
                  $LiveStreamModel->deleteAll(['group_id' => $group_id],false);

                  // Delete Group
                  $GroupModel = TableRegistry::get('groups');
                  $GroupModel->deleteAll(['id' => $group_id],false);

                  // Delete application
                  //$AppModel->delete($appDetail);

                  $AppModel->deleteAll(['group_id' => $group_id],false);
                }
                // Delete forms of user
                $FormListingModel = TableRegistry::get('form_listings');
                $FormListingModel->deleteAll(['owner_id' => $id],false);

                // Delete auth token of user
                $AuthTokenModel = TableRegistry::get('auth_tokens');
                $AuthTokenModel->deleteAll(['user_id' => $id],false);

                // Delete User
                if($this->Users->delete($user)){

                  
                  $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$id])->first();

                  if(!empty($FormSingleData)){
                    $wowza_source_name = $FormSingleData['wowza_source_name'];

                    //Get application name from application table
                    $Applications = TableRegistry::get('Applications'); 
                    $applicationData = $Applications->find('all',['contain'=>['Groups','Users']])->where(['owner_id' =>$FormSingleData['owner_id']])->first();  
                    if(!empty($applicationData)){
                      $application_name  = $applicationData['application_name'];

                      // Call removeRTSPSource api
                      $url = MAIN_RTSP_DOMAIN."api/v1/removeRTSPSource";
                      $postTrasArr = array();
                      $postTrasArr['app_name'] = $application_name;
                      $postTrasArr['source_name'] = $wowza_source_name;
                      $postParmsNew = json_encode($postTrasArr);
                      $postTransResponse = $this->postCurl($url,$postParmsNew);
                      $postTransResponse = json_decode($postTransResponse);
                      //print_r($postTransResponse);die;

                    }
                  }

                    $this->Flash->success(__(user_deleted));
                }
            } 
            else 
            {
                $this->Flash->error(__(userdeletenot));
            }        
            return $this->redirect(['action' => 'index']);
        }
        $this->render('modal/delete-user');
    }



    /**

    
     * Enable/Disable Users
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function changeStatus($id = null, $status = 'off')
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->status = ( $status === 'on' ) ? 1: 0;
        if ($this->Users->save($user)) 
        {
            $this->Flash->success(__(userstatusupdated));
        } 
        else 
        {
            $this->Flash->error(__(userstatusupdatednot));
        }        
        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($user)
    {   
        
        // if ( isset( $user['role'] ) && $user['role'] === 'admin') {
        //     return true;
        // }


        // All registered users can add articles
        if ($this->request->action === 'add') {
            return true;
        }

        // The owner of an user can edit and delete it
        if (in_array($this->request->action, ['edit', 'delete'])) {
            $userId = (int)$this->request->params['pass'][0]; 
            if ($this->Users->isOwnedBy($userId, $user['id'])) {
                return true;
            }
            else
            {
                $this->Flash->error(__(error_403));
                return false;
            }
        }
        return parent::isAuthorized($user);
    }
    function setcookieGDPR(){
      $this->Cookie->write('gdpr_cookies', 'allow');
      return $this->redirect( Router::url( $this->referer(), true ) );
    }


    /*
      Method : deleteUserAccount()
      Title : Used to delete user whole data(user account)
      author : Sandhya Ghatoda
      Created Date : 06-12-2019
    */
    public function deleteUserAccount($id = null){
        $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        if(isset($this->request->data)&& !empty($this->request->data)){   
            if ($user){

              /*
                Title : Save data in archive table before delete target
                author : Sandhya Ghatoda
                Created Date : 05-12-2019
              */  

              $formListingModel = TableRegistry::get("form_listings");
              $formGroups = $formListingModel->find('all')->where(['owner_id'=>$id])->first();

              $owner_id = $id;

              $userModel = TableRegistry::get("users");
              $userData = $userModel->find('all')->where(['id'=>$owner_id])->first();

              $user_id = $owner_id;
              $holder_name = $userData->name;
              

              $conn = ConnectionManager::get('default');

              $comingModel = TableRegistry::get("comming_users");
              $commingUserData = $comingModel->find('all')->where(['form_id' => $formGroups->form_id])->toArray();

              $archiveModel = TableRegistry::get("tbl_archive");

              foreach ($commingUserData as $commingUser) {

              $qwry = "SELECT form_id, IFNULL(SUM(vlike),0) as vlikeSum, IFNULL(SUM(view),0) as viewSum, IFNULL(SUM(comment),0) as commentSum, IFNULL(SUM(love),0) as loveSum, IFNULL(SUM(wow),0) as wowSum, IFNULL(SUM(angry),0) as angrySum, IFNULL(SUM(sad),0) as sadSum, IFNULL(SUM(haha),0) as hahaSum, IFNULL(SUM(share),0) as shareSum from `broadcast_details` where form_id = ".$commingUser->form_id." and target_id = ".$commingUser->id;

                    $followers = $commingUser->followers;
                    $target_name = $commingUser->target_name;

                    $res = $conn->execute( $qwry )->fetchAll('obj');

                    $updateCommingUsers = $archiveModel->newEntity();
                    $updateCommingUsers->user_id = $user_id;
                    $updateCommingUsers->holder_name = $holder_name;
                    $updateCommingUsers->target_name = $target_name;

                    $updateCommingUsers->vlike = $res[0]->vlikeSum;
                    $updateCommingUsers->view = $res[0]->viewSum;
                    $updateCommingUsers->comment = $res[0]->commentSum;
                    $updateCommingUsers->love = $res[0]->loveSum;
                    $updateCommingUsers->wow = $res[0]->wowSum;
                    $updateCommingUsers->angry = $res[0]->angrySum;
                    $updateCommingUsers->sad = $res[0]->sadSum;
                    $updateCommingUsers->haha = $res[0]->hahaSum;
                    $updateCommingUsers->reached = $followers;
                    $updateCommingUsers->share = $res[0]->shareSum;
                    $updateCommingUsers->created_date = getUtcTime();

                    if($archiveModel->save($updateCommingUsers) )
                    {
                       // Save record in archive table
                    }
                  }

                /*
                  End
                */  

                // Get application details
                $AppModel = TableRegistry::get('applications');
                //$appDetail = $AppModel->find('all')->where(['manager_id' => $id])->first();
                $appDetail = $AppModel->find('all')->where(['manager_id' => $id])->toArray();
                if(count($appDetail) > 0){
                  $group_id = $appDetail[0]['group_id'];

                  foreach($appDetail as $app){
                    $this->removeWowzaApp($app['application_name']);                           
                  }
                  
                  $CommingUserModel = TableRegistry::get('comming_users');

                  // Delete registered social users
                  $CommingUserModel->deleteAll(['group_id' => $group_id],false);

                  // Delete broadcasts
                  $LiveBroadcastModel = TableRegistry::get('live_broadcasts');
                  $LiveBroadcastModel->deleteAll(['group_id' => $group_id],false);

                  // Delete streams
                  $LiveStreamModel = TableRegistry::get('live_streams');
                  $LiveStreamModel->deleteAll(['group_id' => $group_id],false);

                  // Delete Group
                  $GroupModel = TableRegistry::get('groups');
                  $GroupModel->deleteAll(['id' => $group_id],false);

                  $broadModel = TableRegistry::get("broadcast_details");
                  $broadModel->deleteAll(['form_id' => $formGroups->form_id],false);

                  // Delete application
                  //$AppModel->delete($appDetail);

                  $AppModel->deleteAll(['group_id' => $group_id],false);
                }
                // Delete forms of user
                $FormListingModel = TableRegistry::get('form_listings');
                $FormListingModel->deleteAll(['owner_id' => $id],false);

                // Delete auth token of user
                $AuthTokenModel = TableRegistry::get('auth_tokens');
                $AuthTokenModel->deleteAll(['user_id' => $id],false);

                // Delete user_plan data of user
                $userPlanModel = TableRegistry::get('user_plans');
                $userPlanModel->deleteAll(['user_id' => $id],false);

                // Delete User
                if($this->Users->delete($user)){

                  
                  $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$id])->first();

                  if(!empty($FormSingleData)){
                    $wowza_source_name = $FormSingleData['wowza_source_name'];

                    //Get application name from application table
                    $Applications = TableRegistry::get('Applications'); 
                    $applicationData = $Applications->find('all',['contain'=>['Groups','Users']])->where(['owner_id' =>$FormSingleData['owner_id']])->first();  
                    if(!empty($applicationData)){
                      $application_name  = $applicationData['application_name'];

                      // Call removeRTSPSource api
                      $url = MAIN_WOWZA_DOMAIN."api/v1/removeRTSPSource";
                      $postTrasArr = array();
                      $postTrasArr['app_name'] = $application_name;
                      $postTrasArr['source_name'] = $wowza_source_name;
                      $postParmsNew = json_encode($postTrasArr);
                      $postTransResponse = $this->postCurl($url,$postParmsNew);
                      $postTransResponse = json_decode($postTransResponse);
                      //print_r($postTransResponse);die;

                    }
                  }

                    $this->Flash->success(__(user_deleted));
                }
            } 
            else 
            {
                $this->Flash->error(__(userdeletenot));
            }        
            
            return $this->redirect(['action' => 'login']);
        }
        $this->render('modal/delete-user-account');
    }

  // Function to change multiplier user to ICH user 
  public function changeUserToIch($id=null, $list_type)
    {
      $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        $this->set('list_type',$list_type);

      if(isset($this->request->data)&& !empty($this->request->data))
      { 

        $UsersModel = TableRegistry::get("users");
        $user_details = $UsersModel->find('all')->where(['id' => $id])->first();
        
        $user_details->registerFrom = "ICH";
        $UsersModel->save($user_details);
            
        
        $this->Flash->success(__(update_succ));
        if($list_type == 1){
        $this->redirect(['action' => 'index']);
        }else if($list_type == 2){
          $this->redirect(['action' => 'expiredUsersList']);
        }else{
          $this->redirect(['action' => 'allUsersList']);
        }
        
      }
      $this->render('modal/change-user-to-ich');
    }

    public function changeUserToMultiplierUser($id=null, $list_type)
    {
      $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        $this->set('list_type',$list_type);

      if(isset($this->request->data)&& !empty($this->request->data))
      { 

        $UsersModel = TableRegistry::get("users");
        $user_details = $UsersModel->find('all')->where(['id' => $id])->first();
        
        $user_details->registerFrom = "-";
        $UsersModel->save($user_details);
            
        
        $this->Flash->success(__(update_succ));
        if($list_type == 1){
        $this->redirect(['action' => 'index']);
        }else if($list_type == 2){
          $this->redirect(['action' => 'expiredUsersList']);
        }else{
          $this->redirect(['action' => 'allUsersList']);
        }
        
      }
      $this->render('modal/change-user-to-multiplier-user');
    }

    public function expiredUsersList(){   
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        $this->set('roles', $roles);
        $query = $this->Users->find('all', ['contain' => ['Groups']]); 
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }     
        if( $userRole == 3 ){
            $query->where(['parent_id' => $this->Auth->user('id')]);
        }else if( $userRole == 1 ){
            $query->where(['role NOT IN' => array('1') ]);
        }elseif( $userRole == 5){
          if($userId==$userSegmentId){
                   $query->where(['role NOT IN' => array('1', '2','5'),'segment_id' =>$userSegment]);
          }else{
             $query->where(['parent_id' => $this->Auth->user('id')]);
          } 
        }else{
            $query->where(['role NOT IN' => array('1', '2') ]);
        }

        $query = clean($query);
        
        $groupsModel = TableRegistry::get("groups");
        $applicationModel = TableRegistry::get("applications");

        foreach ($query as &$user) {
            if ($user->role == 2 OR $user->role == 4){
                $groupData  = $groupsModel->find('all')->where(['owner_id' => $user->parent_id])->first();
                $user->group = $groupData;
            }            
          
          if ($user->role == 4){
               $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$user->parent_id])->first();
             $user->application = $applications;
             }else{  
        $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$user->id])->first();
        $user->application = $applications;               
        }
        }

        //////////get expired users for admin user list///
        $userPlansModel = TableRegistry::get("user_plans");
        if( $userRole == 1 || $userRole == 2){
          $activeUsers = array();
          foreach ($query as &$user) {
            if($user->role == 3 && $user->parent_id == 0){
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$user->id, 'is_expired'=>0])->first();
                if(count($userPlansData) == 0){
                  array_push($activeUsers,$user);
                }
            }else if($user->role == 4 && $user->parent_id != 0){
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$user->parent_id, 'is_expired'=>0])->first();
              if(count($userPlansData) == 0){
                  array_push($activeUsers,$user);
              }
            }else if($user->role == 5){
              $userPlansData = $userPlansModel->find('all')->where(['user_id' =>$user->id, 'is_expired'=>0])->first();
              if(count($userPlansData) == 0){
                  array_push($activeUsers,$user);
              }
            }else{

            }
            
          }
        }
        //////////get expired users for admin user list///

      $segmentModel = TableRegistry::get("segments");
      $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray();

      $FormListingModel = TableRegistry::get("form_listings");  
    $FormSingleData=$FormListingModel->find('list', [
        'keyField' => 'owner_id',
        'valueField' => 'isUnderServer'
        ])->toArray();


        $this->set('segments', $segments);        
        $this->set('FormSingleData', $FormSingleData);
        $this->set('users', $activeUsers);
        $this->set('page', 'invalid_users');
        $this->set('_serialize', ['Users']);
         if($this->Auth->user('segment_id') != 0){
            $segmentModel = TableRegistry::get("segments");
            $segmentsMain  = $segmentModel->find('all')->where(['id' => $this->Auth->user('segment_id')])->first();
           if(!empty($segmentsMain) && ($segmentsMain->name != "fe" || $segmentsMain->name != "Fé")){
              if(!$this->allowRestrictedContent()){
                  $this->Flash->error(__(error_no_plan));
              }
          }
        }else{
          if(!$this->allowRestrictedContent()){
              $this->Flash->error(__(error_no_plan));
          }
        }
    }

  public function changeStatusForExpiredUsers($id = null, $status = 'off')
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->status = ( $status === 'on' ) ? 1: 0;
        if ($this->Users->save($user)) 
        {
            $this->Flash->success(__(userstatusupdated));
        } 
        else 
        {
            $this->Flash->error(__(userstatusupdatednot));
        }        
        return $this->redirect(array("controller" => "users", "action" => "expiredUsersList"));
    }

  public function editExpiredUsers($id = null)
    {   
        
    $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray();    
    $this->set('segments', $segments);
        $segmentDetail = $segmentModel->find('all')->where(['segment_admin' =>$id])->first();
        $user = $this->Users->get( $id, ['contain' => [] ] );
        if ($this->request->is(['patch', 'post', 'put'])) {
      $role = $this->request->data['role'];
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
        if(!empty($segmentDetail) && $role!=5){
          unset($this->request->data['name']);
          $this->request->data['segment_admin']=0;
          $segmentData = $segmentModel->newEntity();
          $segmentData = $segmentModel->patchEntity($segmentData, $this->request->data);
          $segmentData->id = $segmentDetail->id;
          $segmentModel->save($segmentData);
        } 
                $this->Flash->success(__(saved));
                return $this->redirect(array("controller" => "users", "action" => "expiredUsersList"));
            } else {
                $this->Flash->error(__(savenot));
            }
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        unset($roles['1']);

        if($this->Auth->user('role') == 3){
           unset($roles['2']);
           unset($roles['3']);
        }

    if($user->role != 5){
          unset($roles['5']);
    }
        unset($user->password);
        $this->set('roles', $roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    public function changePasswordExpiredUsers($id = null)
    {   
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__(Passwordupdated));
                return $this->redirect(array("controller" => "users", "action" => "expiredUsersList"));
            } else {
                $this->Flash->error(__(savenot));
            }
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        unset($roles['1']);
        unset($user->password);
        $this->set('roles', $roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
    }

    public function deleteExpiredUsers($id = null){
        $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        if(isset($this->request->data)&& !empty($this->request->data)){   
            if ($user){
                // Get application details
                $AppModel = TableRegistry::get('applications');
                //$appDetail = $AppModel->find('all')->where(['manager_id' => $id])->first();
                $appDetail = $AppModel->find('all')->where(['manager_id' => $id])->toArray();
                if(count($appDetail) > 0){
                  $group_id = $appDetail[0]['group_id'];

                  foreach($appDetail as $app){
                    $this->removeWowzaApp($app['application_name']);                           
                  }
                  
                  $CommingUserModel = TableRegistry::get('comming_users');

                  /*$StreamData = $CommingUserModel->find('all')->where(['group_id' => $appDetail->group_id])->toArray();
                  if(count($StreamData) > 0){
                      foreach($StreamData as $key => $value){
                          // Delete app from wowza
                          $this->removeWowzaApp($value['app_name']);                           
                      }
                  }else{
                      $this->removeWowzaApp($appDetail->application_name);                        
                  }*/                   


                  // Delete registered social users
                  $CommingUserModel->deleteAll(['group_id' => $group_id],false);

                  // Delete broadcasts
                  $LiveBroadcastModel = TableRegistry::get('live_broadcasts');
                  $LiveBroadcastModel->deleteAll(['group_id' => $group_id],false);

                  // Delete streams
                  $LiveStreamModel = TableRegistry::get('live_streams');
                  $LiveStreamModel->deleteAll(['group_id' => $group_id],false);

                  // Delete Group
                  $GroupModel = TableRegistry::get('groups');
                  $GroupModel->deleteAll(['id' => $group_id],false);

                  // Delete application
                  //$AppModel->delete($appDetail);

                  $AppModel->deleteAll(['group_id' => $group_id],false);
                }
                // Delete forms of user
                $FormListingModel = TableRegistry::get('form_listings');
                $FormListingModel->deleteAll(['owner_id' => $id],false);

                // Delete auth token of user
                $AuthTokenModel = TableRegistry::get('auth_tokens');
                $AuthTokenModel->deleteAll(['user_id' => $id],false);

                // Delete User
                if($this->Users->delete($user)){

                  
                  $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$id])->first();

                  if(!empty($FormSingleData)){
                    $wowza_source_name = $FormSingleData['wowza_source_name'];

                    //Get application name from application table
                    $Applications = TableRegistry::get('Applications'); 
                    $applicationData = $Applications->find('all',['contain'=>['Groups','Users']])->where(['owner_id' =>$FormSingleData['owner_id']])->first();  
                    if(!empty($applicationData)){
                      $application_name  = $applicationData['application_name'];

                      // Call removeRTSPSource api
                      $url = MAIN_WOWZA_DOMAIN."api/v1/removeRTSPSource";
                      $postTrasArr = array();
                      $postTrasArr['app_name'] = $application_name;
                      $postTrasArr['source_name'] = $wowza_source_name;
                      $postParmsNew = json_encode($postTrasArr);
                      $postTransResponse = $this->postCurl($url,$postParmsNew);
                      $postTransResponse = json_decode($postTransResponse);
                      //print_r($postTransResponse);die;

                    }
                  }

                    $this->Flash->success(__(user_deleted));
                }
            } 
            else 
            {
                $this->Flash->error(__(userdeletenot));
            }        
            return $this->redirect(array("controller" => "users", "action" => "expiredUsersList"));
        }
        $this->render('modal/delete-expired-users');
    }


      public function allUsersList(){   
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        $this->set('roles', $roles);
        $query = $this->Users->find('all', ['contain' => ['Groups']]); 
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }     
        if( $userRole == 3 ){
            $query->where(['parent_id' => $this->Auth->user('id')]);
        }else if( $userRole == 1 ){
            $query->where(['role NOT IN' => array('1') ]);
        }elseif( $userRole == 5){
          if($userId==$userSegmentId){
                   $query->where(['role NOT IN' => array('1', '2','5'),'segment_id' =>$userSegment]);
          }else{
             $query->where(['parent_id' => $this->Auth->user('id')]);
          } 
        }else{
            $query->where(['role NOT IN' => array('1', '2') ]);
        }

        $query = clean($query);
        
        $groupsModel = TableRegistry::get("groups");
        $applicationModel = TableRegistry::get("applications");
        foreach ($query as &$user) {
            if ($user->role == 2 OR $user->role == 4){
                $groupData  = $groupsModel->find('all')->where(['owner_id' => $user->parent_id])->first();
                $user->group = $groupData;
            }            

            if ($user->role == 4){
               $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$user->parent_id])->first();
             $user->application = $applications;
             }else{  
         $applications  = $applicationModel->find('all')->contain(['Users','Groups'])->where(['Users.id' =>$user->id])->first();
         $user->application = $applications;           
        }
        }

      $segmentModel = TableRegistry::get("segments");
      $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray();

      $FormListingModel = TableRegistry::get("form_listings");  
    $FormSingleData=$FormListingModel->find('list', [
        'keyField' => 'owner_id',
        'valueField' => 'isUnderServer'
        ])->toArray();


        $this->set('segments', $segments);        
        $this->set('FormSingleData', $FormSingleData);
        $this->set('users', $query);
        $this->set('page', 'all_users');
        $this->set('_serialize', ['Users']);
         if($this->Auth->user('segment_id') != 0){
            $segmentModel = TableRegistry::get("segments");
            $segmentsMain  = $segmentModel->find('all')->where(['id' => $this->Auth->user('segment_id')])->first();
           if(!empty($segmentsMain) && ($segmentsMain->name != "fe" || $segmentsMain->name != "Fé")){
              if(!$this->allowRestrictedContent()){
                  $this->Flash->error(__(error_no_plan));
              }
          }
        }else{
          if(!$this->allowRestrictedContent()){
              $this->Flash->error(__(error_no_plan));
          }
        }
    }

    public function changeStatusForAllUsers($id = null, $status = 'off')
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->status = ( $status === 'on' ) ? 1: 0;
        if ($this->Users->save($user)) 
        {
            $this->Flash->success(__(userstatusupdated));
        } 
        else 
        {
            $this->Flash->error(__(userstatusupdatednot));
        }        
        return $this->redirect(array("controller" => "users", "action" => "allUsersList"));
    }

  public function editAllUsers($id = null)
    {   
        
    $segmentModel = TableRegistry::get("segments");
        $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray();    
    $this->set('segments', $segments);
        $segmentDetail = $segmentModel->find('all')->where(['segment_admin' =>$id])->first();
        $user = $this->Users->get( $id, ['contain' => [] ] );
        if ($this->request->is(['patch', 'post', 'put'])) {
      $role = $this->request->data['role'];
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
        if(!empty($segmentDetail) && $role!=5){
          unset($this->request->data['name']);
          $this->request->data['segment_admin']=0;
          $segmentData = $segmentModel->newEntity();
          $segmentData = $segmentModel->patchEntity($segmentData, $this->request->data);
          $segmentData->id = $segmentDetail->id;
          $segmentModel->save($segmentData);
        } 
                $this->Flash->success(__(saved));
                return $this->redirect(array("controller" => "users", "action" => "allUsersList"));
            } else {
                $this->Flash->error(__(savenot));
            }
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        unset($roles['1']);

        if($this->Auth->user('role') == 3){
           unset($roles['2']);
           unset($roles['3']);
        }
        
    if($user->role != 5){
          unset($roles['5']);
    }
        unset($user->password);
        $this->set('roles', $roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
        if(!$this->allowRestrictedContent()){
            $this->Flash->error(__(error_no_plan));
        }
    }

    public function changePasswordAllUsers($id = null)
    {   
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__(Passwordupdated));
                return $this->redirect(array("controller" => "users", "action" => "allUsersList"));
            } else {
                $this->Flash->error(__(savenot));
            }
        }
        //$roles = Configure::read('sonoconfig.roles');
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        unset($roles['1']);
        unset($user->password);
        $this->set('roles', $roles);
        $this->set(compact('user'));
        $this->set('_serialize', ['application']);
    }

    public function deleteAllUsers($id = null){
        $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        if(isset($this->request->data)&& !empty($this->request->data)){   
            if ($user){
                // Get application details
                $AppModel = TableRegistry::get('applications');
                //$appDetail = $AppModel->find('all')->where(['manager_id' => $id])->first();
                $appDetail = $AppModel->find('all')->where(['manager_id' => $id])->toArray();
                if(count($appDetail) > 0){
                  $group_id = $appDetail[0]['group_id'];

                  foreach($appDetail as $app){
                    $this->removeWowzaApp($app['application_name']);                           
                  }
                  
                  $CommingUserModel = TableRegistry::get('comming_users');

                  /*$StreamData = $CommingUserModel->find('all')->where(['group_id' => $appDetail->group_id])->toArray();
                  if(count($StreamData) > 0){
                      foreach($StreamData as $key => $value){
                          // Delete app from wowza
                          $this->removeWowzaApp($value['app_name']);                           
                      }
                  }else{
                      $this->removeWowzaApp($appDetail->application_name);                        
                  }*/                   


                  // Delete registered social users
                  $CommingUserModel->deleteAll(['group_id' => $group_id],false);

                  // Delete broadcasts
                  $LiveBroadcastModel = TableRegistry::get('live_broadcasts');
                  $LiveBroadcastModel->deleteAll(['group_id' => $group_id],false);

                  // Delete streams
                  $LiveStreamModel = TableRegistry::get('live_streams');
                  $LiveStreamModel->deleteAll(['group_id' => $group_id],false);

                  // Delete Group
                  $GroupModel = TableRegistry::get('groups');
                  $GroupModel->deleteAll(['id' => $group_id],false);

                  // Delete application
                  //$AppModel->delete($appDetail);

                  $AppModel->deleteAll(['group_id' => $group_id],false);
                }
                // Delete forms of user
                $FormListingModel = TableRegistry::get('form_listings');
                $FormListingModel->deleteAll(['owner_id' => $id],false);

                // Delete auth token of user
                $AuthTokenModel = TableRegistry::get('auth_tokens');
                $AuthTokenModel->deleteAll(['user_id' => $id],false);

                // Delete User
                if($this->Users->delete($user)){

                  
                  $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$id])->first();

                  if(!empty($FormSingleData)){
                    $wowza_source_name = $FormSingleData['wowza_source_name'];

                    //Get application name from application table
                    $Applications = TableRegistry::get('Applications'); 
                    $applicationData = $Applications->find('all',['contain'=>['Groups','Users']])->where(['owner_id' =>$FormSingleData['owner_id']])->first();  
                    if(!empty($applicationData)){
                      $application_name  = $applicationData['application_name'];

                      // Call removeRTSPSource api
                      $url = MAIN_WOWZA_DOMAIN."api/v1/removeRTSPSource";
                      $postTrasArr = array();
                      $postTrasArr['app_name'] = $application_name;
                      $postTrasArr['source_name'] = $wowza_source_name;
                      $postParmsNew = json_encode($postTrasArr);
                      $postTransResponse = $this->postCurl($url,$postParmsNew);
                      $postTransResponse = json_decode($postTransResponse);
                      //print_r($postTransResponse);die;

                    }
                  }

                    $this->Flash->success(__(user_deleted));
                }
            } 
            else 
            {
                $this->Flash->error(__(userdeletenot));
            }        
            return $this->redirect(array("controller" => "users", "action" => "allUsersList"));
        }
        $this->render('modal/delete-any-users');
    }

   
    public function accountApprovalList(){  

        // echo "<pre>";print_r("hii");die; 
        if($this->request->session()->read('language') == "en_US"){
          $roles = Configure::read('sonoconfig.roles_en');
        }else if($this->request->session()->read('language') == "en_SP"){
          $roles = Configure::read('sonoconfig.roles_es');
        }else{
          $roles = Configure::read('sonoconfig.roles_pt');
        } 
        $this->set('roles', $roles);
        $query = $this->Users->find('all', ['contain' => ['Groups']])->where(['is_pipefy_verified' => 0]); 
        $userId = $this->Auth->user('id');
        $userRole = $this->Auth->user('role');
        $userSegment = $this->Auth->user('segment_id');
        $SegmentModel = TableRegistry::get('Segments');
        $SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
        $userSegmentId=0;
        if(!empty($SegmentData)){
          $userSegmentId = $SegmentData->segment_admin;
        }     
        if( $userRole == 3 ){
            $query->where(['parent_id' => $this->Auth->user('id')]);
        }else if( $userRole == 1 ){
            $query->where(['role NOT IN' => array('1') ]);
        }elseif( $userRole == 5){
          if($userId==$userSegmentId){
                   $query->where(['role NOT IN' => array('1', '2','5'),'segment_id' =>$userSegment]);
          }else{
             $query->where(['parent_id' => $this->Auth->user('id')]);
          } 
        }else{
            $query->where(['role NOT IN' => array('1', '2') ]);
        }

        $query = clean($query);
        
        if( $userRole == 1 || $userRole == 2){
          $activeUsers = array();
          foreach ($query as &$user) {
              array_push($activeUsers,$user);
          }
        }
        //////////get active users for admin user list///

        $usersModel = TableRegistry::get("Users");
        $user_details = $usersModel->find('all')->where(['id' => $userId])->first();

        
        $this->set("isExpired", 0);
        $this->set("user_type", $user_details->registerFrom);

      $segmentModel = TableRegistry::get("segments");
      $segments  = $segmentModel->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->where(['status' => 1])->toArray();

        $this->set('segments', $segments);        
        if( $userRole == 1 || $userRole == 2){     
          $this->set('users', $activeUsers);
        }else{
        $this->set('users', $query);
        }
        $this->set('page', 'pipefy_users');
        $this->set('_serialize', ['Users']);
         if($this->Auth->user('segment_id') != 0){
            $segmentModel = TableRegistry::get("segments");
            $segmentsMain  = $segmentModel->find('all')->where(['id' => $this->Auth->user('segment_id')])->first();
           if(!empty($segmentsMain) && ($segmentsMain->name != "fe" || $segmentsMain->name != "Fé")){
              if(!$this->allowRestrictedContent()){
                  $this->Flash->error(__(error_no_plan));
              }
          }
        }else{
          if(!$this->allowRestrictedContent()){
              $this->Flash->error(__(error_no_plan));
          }
        }
    }

    /*public function accountApprovalStatus($id = null, $status = 'off')
    {
        $user = $this->Users->get($id);
        $user->is_pipefy_verified = ( $status === 'on' ) ? 1: 0;

        $webservice = 1;
        $register_from = 'WebApp';

        if ($this->Users->save($user)) {

        $appArr = explode('@',$user->email);
        $appName = clean($appArr[0]);
        $result = $this->addApplication($appName, $user->id);
        if($result){
            $MultilikeForm = $this->addMultilikeForm($user->id, $user->email, $appName, $user, 'en_US');
            $form = $this->addForm($user->id, $user->email, $appName, $user, 'en_US');
        if($form){

              $AppMdl = TableRegistry::get('Applications'); 
              $appDt = $AppMdl->find("all")->where(['manager_id'=> $user->id])->first();

              $FormMdl = TableRegistry::get("form_listings");  
              $FormDt = $FormMdl->find('all')->where(['owner_id'=>$user->id])->first();

              $customTargetNameNew = "MasterRepeater".rand(1,999).rand(1,999);
              $this->appsChainingForPlayerMapp($appDt->application_name, $customTargetNameNew, $FormDt->wowza_source_name, "playermapp", $FormDt->isRTSPUser);
             
              // Added webservice user on WP start
              if(!empty($webservice) && $webservice == 1){

                 //Save Trial Plan
                  $start_date = date('Y-m-d H:i:s');
                  $expiry_date = date("Y-m-d", strtotime($start_date ." +7 day") );
                  $target_limit = 30;
                  if(!empty($user->segment_id)){
                        $SegmentModel = TableRegistry::get('segments');
                        
                        $segment_id = $user->segment_id;
                        $SegmentData = $SegmentModel->find("all")->where(['id'=> $segment_id])->first();

                        if(!empty($SegmentData)){
                          if($SegmentData['trial_days'] && $SegmentData['target_limit']){
                            $expiry_date = date("Y-m-d", strtotime($start_date ." +".$SegmentData['trial_days']." day") );
                            $target_limit = $SegmentData['target_limit'];                        
                          }
                        }
                  }

                  $plan_data = array('user_id' => $user->id, 'order_id'=> 0, 'product_id'=> 0, 'product_name'=> 'Trial', 'target_limit'=> $target_limit, 'start_date'=> $start_date, 'expiry_date'=> $expiry_date, 'payment_date'=> $start_date, 'payment_complete_date'=> $start_date);

                  $plan_url = HTTP_ROOT. 'api/saveUserPlanDetails';

                  $headers = array(
                    'Content-Type:application/json'
                  );

                  $plan_result = $this->postCurl( $plan_url, json_encode($plan_data), $headers);
                 
                  // Update user details  
                  $update_data = array('user_id' => $user->id);        
                  $update_url = HTTP_ROOT. 'api/updateUserDetails';
                  $update_result = $this->postCurl($update_url, json_encode($update_data), $headers);

                  //starts here  
                  if(!empty($register_from) && $register_from == 'WebApp'){                    
                    return $this->successHandler(1, REGISTER_SUCCESS, 200);
                  }else{
                    $token = bin2hex(openssl_random_pseudo_bytes(16));
                    $AuthTokenModel = TableRegistry::get('auth_tokens');          
                    $authDetail = $AuthTokenModel->find('all')->where(['user_id'=> $user->id])->first();
                    $authData = $AuthTokenModel->newEntity();
                    if(!empty($authDetail)){
                      $authData->id = $authDetail->id;
                    }
                    $authData->user_id = $user->id;
                    $authData->token = $token;
  
                    //save token
                    if($AuthTokenModel->save($authData)){
                      // Get manager form details
                          $FormListingModel = TableRegistry::get("form_listings");  
                          $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$user->id])->first();
                          if(!empty($FormSingleData)){
                          $stream_name = $FormSingleData['wowza_source_name'];
                          $Applications = TableRegistry::get('Applications'); 
                          $applicationData = $Applications->find('all',['contain'=>['Groups','Users']])->where(['owner_id' =>$FormSingleData['owner_id']])->first();  
                          if(!empty($applicationData)){

                            if($FormSingleData['isUnderServer'] == 1){
                                $rtmp_url = RTMP_DOMAIN_UNDER.$applicationData['application_name'];
                                }else{
                            $rtmp_url = RTMP_DOMAIN.$applicationData['application_name'];
                          }
                        }
                      }

                          $protocal_type = "rtmp://";
                          $wowzaIp = WOWZA_IP;
                          $wowzaLivePort = "1935";
                          $wowzaApplicationName = $applicationData['application_name'];
                          $wowzaStreamName = $stream_name;
                          $formId = $FormSingleData['form_id'];
                          $form_url = HTTP_ROOT."commingusers/view-form?id=".$formId;

                      // Get user active plans
                      $plans = $this->getUserActivePlans($user->id);
                      $userDetails = $UsersModel->find('all')->where(['id'=> $user->id])->first();        
                      $privateplayer = HTTP_ROOT."player/index.html?appname=".$applicationData['application_name']."&streamname=private".$FormSingleData['wowza_source_name'];
                      $privateStreamKey = "private".$FormSingleData['wowza_source_name'];
                      $data = array("user"=> $userDetails, "token"=> $token, "rtmp_url"=> $rtmp_url, "wowzaStreamName"=> $wowzaStreamName, "wowzaApplicationName"=> $wowzaApplicationName, "wowzaLivePort"=> $wowzaLivePort, "wowzaIp"=> $wowzaIp, "protocal_type"=> $protocal_type, 'form_url'=> $form_url, 'plans'=> $plans,'private_player'=>$privateplayer,'privatewowzaStreamName' => $privateStreamKey);
                      return $this->successHandler(1, REGISTER_SUCCESS, $data);
                    }else{
                      $this->deleteUser($user->id);
                      $this->Flash->error(__(userstatusupdatednot));
                    }
                  }  //starts here  


              } // Added webservice user on WP end
       
             
         } //form if end
       } //form if end

                $this->Flash->success(__(Passwordupdated));
                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__(savenot));
            }

    }*/

    /* Delete function for Approval User
    Aparna Sharma
    2020-08-10*/
     public function deleteApprove($id = null){
        $user = $this->Users->get($id);
        $this->viewBuilder()->layout('');
        $this->set('user',$user);
        $this->set('userId',$user->id);
        if(isset($this->request->data)&& !empty($this->request->data)){   
            if ($user){
                // Get application details
                $AppModel = TableRegistry::get('applications');
                //$appDetail = $AppModel->find('all')->where(['manager_id' => $id])->first();
                $appDetail = $AppModel->find('all')->where(['manager_id' => $id])->toArray();
                if(count($appDetail) > 0){
                  $group_id = $appDetail[0]['group_id'];

                  foreach($appDetail as $app){
                    $this->removeWowzaApp($app['application_name']);                           
                  }
                  
                  $CommingUserModel = TableRegistry::get('comming_users');

                  /*$StreamData = $CommingUserModel->find('all')->where(['group_id' => $appDetail->group_id])->toArray();
                  if(count($StreamData) > 0){
                      foreach($StreamData as $key => $value){
                          // Delete app from wowza
                          $this->removeWowzaApp($value['app_name']);                           
                      }
                  }else{
                      $this->removeWowzaApp($appDetail->application_name);                        
                  }*/                   


                  // Delete registered social users
                  $CommingUserModel->deleteAll(['group_id' => $group_id],false);


                  //Delate default room
                  $RoomModel = TableRegistry::get('rooms');
                  $RoomModel->deleteAll(['application_id' => $group_id,'roomName' => 'room #1'],false);

                  // Delete broadcasts
                  $LiveBroadcastModel = TableRegistry::get('live_broadcasts');
                  $LiveBroadcastModel->deleteAll(['group_id' => $group_id],false);

                  // Delete streams
                  $LiveStreamModel = TableRegistry::get('live_streams');
                  $LiveStreamModel->deleteAll(['group_id' => $group_id],false);

                  // Delete Group
                  $GroupModel = TableRegistry::get('groups');
                  $GroupModel->deleteAll(['id' => $group_id],false);

                  // Delete application
                  //$AppModel->delete($appDetail);

                  $AppModel->deleteAll(['group_id' => $group_id],false);
                }
                // Delete forms of user
                $FormListingModel = TableRegistry::get('form_listings');
                $FormListingModel->deleteAll(['owner_id' => $id],false);

                // Delete auth token of user
                $AuthTokenModel = TableRegistry::get('auth_tokens');
                $AuthTokenModel->deleteAll(['user_id' => $id],false);

                // Delete User
                if($this->Users->delete($user)){

                  
                  $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$id])->first();

                  if(!empty($FormSingleData)){
                    $wowza_source_name = $FormSingleData['wowza_source_name'];

                    //Get application name from application table
                    $Applications = TableRegistry::get('Applications'); 
                    $applicationData = $Applications->find('all',['contain'=>['Groups','Users']])->where(['owner_id' =>$FormSingleData['owner_id']])->first();  
                    if(!empty($applicationData)){
                      $application_name  = $applicationData['application_name'];

                      // Call removeRTSPSource api
                      $url = MAIN_RTSP_DOMAIN."api/v1/removeRTSPSource";
                      $postTrasArr = array();
                      $postTrasArr['app_name'] = $application_name;
                      $postTrasArr['source_name'] = $wowza_source_name;
                      $postParmsNew = json_encode($postTrasArr);
                      $postTransResponse = $this->postCurl($url,$postParmsNew);
                      $postTransResponse = json_decode($postTransResponse);
                      //print_r($postTransResponse);die;

                    }
                  }

                    $this->Flash->success(__(user_deleted));
                }
            } 
            else 
            {
                $this->Flash->error(__(userdeletenot));
            }        
            return $this->redirect(['action' => 'accountApprovalList']);
        }
        $this->render('modal/delete-approve-user');
    }
  
 
}
?>