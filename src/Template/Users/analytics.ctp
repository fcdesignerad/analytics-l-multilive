<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hello Analytics Reporting API V4</title>
	<meta name="google-signin-client_id" content="862535078023-ll5uqviqs4orcdjdra353i8gi92q3dua.apps.googleusercontent.com">
	<meta name="google-signin-scope" content="https://www.googleapis.com/auth/analytics.readonly">
	<script src="https://www.amcharts.com/lib/4/core.js"></script>
	<script src="https://www.amcharts.com/lib/4/charts.js"></script>
	<script src="https://www.amcharts.com/lib/4/maps.js"></script>
	<script src="https://www.amcharts.com/lib/4/geodata/worldLow.js"></script>
	<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
	<!-- <script src="https://www.amcharts.com/lib/4/geodata/worldHigh.js"></script> -->

	<?php if($language == "English"){  ?>

		<script src="https://www.amcharts.com/lib/4/geodata/lang/EN.js"></script>
		<script> worldMapCountrysName = am4geodata_lang_EN;  </script>

	<?php } else if($language == "Spanish"){  ?>

		<script src="https://www.amcharts.com/lib/4/geodata/lang/ES.js"></script>
		<script> worldMapCountrysName = am4geodata_lang_ES; </script>

	<?php } else {  ?>

		<script src="https://www.amcharts.com/lib/4/geodata/lang/PT.js"></script>
		<script> worldMapCountrysName = am4geodata_lang_PT; </script>

	<?php } ?>

</head>
<body>

	

	<div id = "charts_wrapper">

		<div class = "row buttons-row">

			<div id = "header_wrapper">
				<h1> <?= __(analyticsReport) ?> </h1>
			</div>

			<div class = "buttons-wrapper">
				<button class = "data-report" id = "data_report_1" data-days = "1" data-analytics-date = "yesterday" >1 <?= __(days) ?> </button>
			</div>
			
			<div class = "buttons-wrapper">
				<button class = "data-report" id = "data_report_7" data-days = "7" data-analytics-date = "7daysAgo">7 <?= __(days) ?></button>
			</div>

			<div class = "buttons-wrapper">
				<button class = "data-report" id = "data_report_30" data-days = "30" data-analytics-date = "30daysAgo">30 <?= __(days) ?></button>
			</div>

			<div class = "buttons-wrapper">
				<button class = "data-report" id = "data_report_60" data-days = "60" data-analytics-date = "60daysAgo">60 <?= __(days) ?></button>
			</div>

			<div class = "buttons-wrapper">
				<button class = "data-report" id = "data_report_120" data-days = "89" data-analytics-date = "89daysAgo">90 <?= __(days) ?></button>
			</div>
			
		</div>

		<div class = "chart-wrapper" id = "user_container">

			<div>
				<h3> <?= __(users) ?> </h3>
				<h3 id = "user_total_number"> </h3>
				<hr>
				<h3> <?= __(newUsers) ?> </h3>
				<h3 id = "new_user_total_number"> </h3>
				<hr>
				<!-- <h3> <?= __(sessions) ?> </h3>
				<h3 id = "sessions_total_number"> </h3>
				<hr> -->
			</div>

			<div>

			<div id = "device_number_container">
				<h3>Mobile</h3>
				<h3 id = "mobile_number_wrapper"></h3>
				<hr>
				<h3>Desktop</h3>
				<h3 id = "desktop_number_wrapper"></h3>
			</div>
			<div class = "chart" id = "device_chart"></div>

			</div>

		</div>

		<div class = "chart-wrapper" id = "total_session_chart_container">
			<h3>Total</h3>
			<div class = "chart" id = "total_session_chart"></div>
		</div>
		
		<div class = "chart-wrapper" id = "world_chart_container">
			<h3> <?= __(worldMap) ?> </h3>
			<div class = "chart" id = "world_chart"></div>
		</div>

		<div class = "chart-wrapper triple-column" id = "age_container">
			<h3> <?= __(age) ?> </h3>

			<div id = "age_chart"></div>

			<div class = "analytic-list-wrapper" id = "age_list_wrapper">
				<ul class = "analytic-list" id = "age_list"></ul>
			</div>

		</div>

		<div class = "chart-wrapper triple-column" id = "country_container">
			<h3> <?= __(countries) ?> </h3>
			<h3 id = "country_total_number"> </h3>

			<div class = "analytic-list-wrapper" id = "country_lista_wrapper">
				<ul class = "analytic-list"  id = "country_lista"></ul>
			</div>			
		</div>

		<div class = "chart-wrapper triple-column" id = "state_container">
			<h3> <?= __(states) ?> </h3>
			<h3 id = "state_total_number"> </h3>

			<div class = "analytic-list-wrapper" id = "state_list_wrapper">
				<ul class = "analytic-list" id = "state_list"></ul>
			</div>

		</div>

		<div class = "chart-wrapper triple-column" id = "gender_container">
			<h3> <?= __(gender) ?> </h3>

			<div id = "gender_chart"></div>

			<div class = "analytic-list-wrapper" id = "gender_list_wrapper">
				<ul class = "analytic-list" id = "gender_list"></ul>
			</div>

		</div>

	</div>

	<p style = "opacity: 0;">
		Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
	</p>

</body>
</html>

<script src="https://apis.google.com/js/client:platform.js"></script>
<!-- <script src="https://apis.google.com/js/api.js"></script> -->

<script>
	
	/** Analytics */
  	var VIEW_ID = '204709954';
	var form_id = "4331";

	var totalUsers = 0;

	var reportTypes = ['usersPerDay', 'country', 'age', 'states', 'gender'];

	/** Countries */

	var worldMap;

	delete worldMapCountrysName.AQ;
	var countries = [];
	var country_abbreviations = {
		'Afghanistan':									'AF',
		'Aland Islands':								'AX',
		'Albania':										'AL',
		'Algeria':										'DZ',
		'American Samoa':								'AS',
		'Andorra':										'AD',
		'Angola':										'AO',
		'Anguilla':										'AI',
		// 'Antarctica':									'AQ',
		'Antigua and Barbuda':							'AG',
		'Argentina':									'AR',
		'Armenia':										'AM',
		'Aruba':										'AW',
		'Australia':									'AU',
		'Austria':										'AT',
		'Azerbaijan':									'AZ',
		'Bahamas':										'BS',
		'Bahrain':										'BH',
		'Bangladesh':									'BD',
		'Barbados':										'BB',
		'Belarus':										'BY',
		'Belgium':										'BE',
		'Belize':										'BZ',
		'Benin':										'BJ',
		'Bermuda':										'BM',
		'Bhutan':										'BT',
		'Bolivia':										'BO',
		'Bonaire':										'BQ',
		'Bosnia and Herzegovina':						'BA',
		'Botswana':										'BW',
		'Bouvet Island':								'BV',
		'Brazil':										'BR',
		'British Indian Ocean Territory':				'IO',
		'British Virgin Islands':						'VG',
		'Brunei':										'BN',
		'Bulgaria':										'BG',
		'Burkina Faso':									'BF',
		'Burundi':										'BI',
		'Cambodia':										'KH',
		'Cameroon':										'CM',
		'Canada':										'CA',
		'Cape Verde':									'CV',
		'Cayman Islands':								'KY',
		'Central African Republic':						'CF',
		'Chad':											'TD',
		'Chile':										'CL',
		'China':										'CN',
		'Christmas Island':								'CX',
		'Cocos Islands':								'CC',
		'Colombia':										'CO',
		'Comoros':										'KM',
		'Cook Islands':									'CK',
		'Costa Rica':									'CR',
		'Croatia':										'HR',
		'Cuba':											'CU',
		'Curacao':										'CW',
		'Cyprus':										'CY',
		'Czech Republic':								'CZ',
		'Democratic Republic of the Congo':				'CD',
		'Denmark':										'DK',
		'Djibouti':										'DJ',
		'Dominica':										'DM',
		'Dominican Republic':							'DO',
		'East Timor':									'TL',
		'Ecuador':										'EC',
		'Egypt':										'EG',
		'El Salvador':									'SV',
		'Equatorial Guinea':							'GQ',
		'Eritrea':										'ER',
		'Estonia':										'EE',
		'Ethiopia':										'ET',
		'Falkland Islands':								'FK',
		'Faroe Islands':								'FO',
		'Fiji':											'FJ',
		'Finland':										'FI',
		'France':										'FR',
		'French Guiana':								'GF',
		'French Polynesia':								'PF',
		'French Southern Territories':					'TF',
		'Gabon':										'GA',
		'Gambia':										'GM',
		'Georgia':										'GE',
		'Germany':										'DE',
		'Ghana':										'GH',
		'Gibraltar':									'GI',
		'Greece':										'GR',
		'Greenland':									'GL',
		'Grenada':										'GD',
		'Guadeloupe':									'GP',
		'Guam':											'GU',
		'Guatemala':									'GT',
		'Guernsey':										'GG',
		'Guinea':										'GN',
		'Guinea-Bissau':								'GW',
		'Guyana':										'GY',
		'Haiti':										'HT',
		// 'Heard Island and McDonald Islands':			'HM',
		'Honduras':										'HN',
		'Hong Kong':									'HK',
		'Hungary':										'HU',
		'Iceland':										'IS',
		'India':										'IN',
		'Indonesia':									'ID',
		'Iran':											'IR',
		'Iraq':											'IQ',
		'Ireland':										'IE',
		'Isle of Man':									'IM',
		'Israel':										'IL',
		'Italy':										'IT',
		'Ivory Coast':									'CI',
		'Jamaica':										'JM',
		'Japan':										'JP',
		'Jersey':										'JE',
		'Jordan':										'JO',
		'Kazakhstan':									'KZ',
		'Kenya':										'KE',
		'Kiribati':										'KI',
		'Kosovo':										'XK',
		'Kuwait':										'KW',
		'Kyrgyzstan':									'KG',
		'Laos':											'LA',
		'Latvia':										'LV',
		'Lebanon':										'LB',
		'Lesotho':										'LS',
		'Liberia':										'LR',
		'Libya':										'LY',
		'Liechtenstein':								'LI',
		'Lithuania':									'LT',
		'Luxembourg':									'LU',
		'Macao':										'MO',
		'Macedonia':									'MK',
		'Madagascar':									'MG',
		'Malawi':										'MW',
		'Malaysia':										'MY',
		'Maldives':										'MV',
		'Mali':											'ML',
		'Malta':										'MT',
		'Marshall Islands':								'MH',
		'Martinique':									'MQ',
		'Mauritania':									'MR',
		'Mauritius':									'MU',
		'Mayotte':										'YT',
		'Mexico':										'MX',
		'Micronesia':									'FM',
		'Moldova':										'MD',
		'Monaco':										'MC',
		'Mongolia':										'MN',
		'Montenegro':									'ME',
		'Montserrat':									'MS',
		'Morocco':										'MA',
		'Mozambique':									'MZ',
		'Myanmar':										'MM',
		'Namibia':										'NA',
		'Nauru':										'NR',
		'Nepal':										'NP',
		'Netherlands':									'NL',
		'Netherlands Antilles':							'AN',
		'New Caledonia':								'NC',
		'New Zealand':									'NZ',
		'Nicaragua':									'NI',
		'Niger':										'NE',
		'Nigeria':										'NG',
		'Niue':											'NU',
		'Norfolk Island':								'NF',
		'North Korea':									'KP',
		'Northern Mariana Islands':						'MP',
		'Norway':										'NO',
		'Oman':											'OM',
		'Pakistan':										'PK',
		'Palau':										'PW',
		'Palestinian Territory':						'PS',
		'Panama':										'PA',
		'Papua New Guinea':								'PG',
		'Paraguay':										'PY',
		'Peru':											'PE',
		'Philippines':									'PH',
		'Pitcairn':										'PN',
		'Poland':										'PL',
		'Portugal':										'PT',
		'Puerto Rico':									'PR',
		'Qatar':										'QA',
		'Republic of the Congo':						'CG',
		'Reunion':										'RE',
		'Romania':										'RO',
		'Russia':										'RU',
		'Rwanda':										'RW',
		'Saint Barthelemy':								'BL',
		'Saint Helena':									'SH',
		'Saint Kitts and Nevis':						'KN',
		'Saint Lucia':									'LC',
		'Saint Martin':									'MF',
		'Saint Pierre and Miquelon':					'PM',
		'Saint Vincent and the Grenadines':				'VC',
		'Samoa':										'WS',
		'San Marino':									'SM',
		'Sao Tome and Principe':						'ST',
		'Saudi Arabia':									'SA',
		'Senegal':										'SN',
		'Serbia':										'RS',
		// 'Serbia and Montenegro':						'CS',
		'Seychelles':									'SC',
		'Sierra Leone':									'SL',
		'Singapore':									'SG',
		'Sint Maarten':									'SX',
		'Slovakia':										'SK',
		'Slovenia':										'SI',
		'Solomon Islands':								'SB',
		'Somalia':										'SO',
		'South Africa':									'ZA',
		// 'South Georgia and the South Sandwich Islands':	'GS',
		'South Korea':									'KR',
		'South Sudan':									'SS',
		'Spain':										'ES',
		'Sri Lanka':									'LK',
		'Sudan':										'SD',
		'Suriname':										'SR',
		'Svalbard and Jan Mayen':						'SJ',
		'Swaziland':									'SZ',
		'Sweden':										'SE',
		'Switzerland':									'CH',
		'Syria':										'SY',
		'Taiwan':										'TW',
		'Tajikistan':									'TJ',
		'Tanzania':										'TZ',
		'Thailand':										'TH',
		'Togo':											'TG',
		'Tokelau':										'TK',
		'Tonga':										'TO',
		'Trinidad and Tobago':							'TT',
		'Tunisia':										'TN',
		'Turkey':										'TR',
		'Turkmenistan':									'TM',
		'Turks and Caicos Islands':						'TC',
		'Tuvalu':										'TV',
		'U.S. Virgin Islands':							'VI',
		'Uganda':										'UG',
		'Ukraine':										'UA',
		'United Arab Emirates':							'AE',
		'United Kingdom':								'GB',
		'United States':								'US',
		// 'United States Minor Outlying Islands':			'UM',
		'Uruguay':										'UY',
		'Uzbekistan':									'UZ',
		'Vanuatu':										'VU',
		'Vatican':										'VA',
		'Venezuela':									'VE',
		'Vietnam':										'VN',
		'Wallis and Futuna':							'WF',
		'Western Sahara':								'EH',
		'Yemen':										'YE',
		'Zambia':										'ZM',
		'Zimbabwe':										'ZW',
	};

	/** Device */

	var deviceChart;

	/** State */

	var brazilStates = {
		'Acre': 0,
		'Alagoas': 0,
		'Amapá': 0,
		'Amazonas': 0,
		'Bahia': 0,
		'Ceará': 0,
		'Espírito Santo': 0,
		'Goiás': 0,
		'Maranhão': 0,
		'Mato Grosso': 0,
		'Mato Grosso do Sul': 0,
		'Minas Gerais': 0,
		'Pará': 0,
		'Paraíba': 0,
		'Paraná': 0,
		'Pernambuco': 0,
		'Piauí': 0,
		'Rio de Janeiro': 0,
		'Rio Grande do Norte': 0,
		'Rio Grande do Sul': 0,
		'Rondônia': 0,
		'Roraima': 0,
		'Santa Catarina': 0,
		'São Paulo': 0,
		'Sergipe': 0,
		'Tocantins': 0,
		'Distrito Federal': 0,
	};

	/** Age */

	var ageChart;
	var ageBrackets = {
		'18-24': 0,
		'25-34': 0,
		'35-44': 0,
		'45-54': 0,
		'55-64': 0,
		'65+': 0,
	};
	var ageBracketsAux = Object.assign({}, ageBrackets);

	/** Gender */

	var genderChart;
	var genders = {
		'male': 0,
		'female': 0,
	};
	var gendersAux = Object.assign({}, genders);

	/** Users */

	var usersChart;
	var usersAverageLine;


	$(document).on('click', '.data-report', function(ev){

		$('.data-report').removeClass('active');
		$(this).addClass('active');

		newAnalyticsRequest('all', $(this).attr('data-analytics-date'), 'today');
	});

	$(document).on('click', '.chart-wrapper a', function(ev){
		ev.preventDefault();
	})

	$(document).ready(function(ev){
		initializeDashboard();
	});

	function initializeDashboard(){

		initiateLoaders();

		var deltaT = 200;

		buildInitialCharts(deltaT);
		
	}

	function buildInitialCharts(deltaT = 100){
		
		setTimeout(function(){
			buildusersPerDayChart([]);
			// removeSmokeScreen('world_chart_container');

			setTimeout(function(){
				animateUsersCount([]);
				buildDeviceChartAndList([]);
				// removeSmokeScreen('user_container');

				setTimeout(function(){
					buildGenderChartAndList([]);
					// removeSmokeScreen('country_container');
					buildAgeChartAndList([]);
					// removeSmokeScreen('age_container');

					setTimeout(function(){
						
						// removeSmokeScreen('gender_container');
						buildWorldMap([]);
						createCountryList();
						createStatesList([]);
						hideAmchartsLogo();
						stopLoadingProcess();

						setTimeout(function(){
							$('#data_report_30').click();
						}, deltaT + deltaT);

					}, deltaT);

				}, deltaT);

			}, deltaT);

		}, deltaT);


	}

	function newAnalyticsRequest(type, startDate, endDate){

		if(type == 'all'){
			initiateLoaders();

			var id = getWrapperIdByReportType('usersAndDevices');

			$.ajax({
				url: `/app/users/buildAnalyticsRequest/usersAndDevices/${startDate}/${endDate}`,
				type: "POST",
			}).done(function(data) {

				totalUsers = ( data['total_users'] == undefined ) ? 0 : data['total_users'];

				buildNewReportByType(data, 'usersAndDevices');
				removeSmokeScreen(id);

				$.each(reportTypes, function(index, value){

					var id = getWrapperIdByReportType(value);

					$.ajax({
						url: `/app/users/buildAnalyticsRequest/${value}/${startDate}/${endDate}`,
						type: "POST",
					}).done(function(data) {

						if(value == 'usersAndDevices'){
							totalUsers = ( data['total_users'] == undefined ) ? 0 : data['total_users'];
						}

						buildNewReportByType(data, value);
						removeSmokeScreen(id);

					});

				});

			});

		}
		else{
			var id = getWrapperIdByReportType(type);
			addSmokeScreen(id);

			$.ajax({
				url: `/app/users/buildAnalyticsRequest/${type}/${startDate}/${endDate}`,
				type: "POST",
			}).done(function(data) {

				if(value == 'usersAndDevices'){
					totalUsers = ( data['total_users'] == undefined ) ? 0 : data['total_users'];
				}

				buildNewReportByType(data, type);
				removeSmokeScreen(id);

			});

		}
		
	}

	function teste(type, startDate = 'yesterday', endDate = 'today'){

		var a = {filterCountry: 'Brazil'};

		// var id = getWrapperIdByReportType(type);
		// addSmokeScreen(id);

		$.ajax({
			url: `/app/users/buildAnalyticsRequest/${type}/${startDate}/${endDate}/Brazil`,
			type: "POST",
		}).done(function(data) {

			// if(value == 'country'){
			// 	totalUsers = data['total'];
			// }

			console.log(data);
			
			// buildNewReportByType(data, type);
			// removeSmokeScreen(id);

		});
				
	}

	function getWrapperIdByReportType(type){

		var id;

		switch(type){
			case 'country':
				id = 'world_chart_container .smoke-screen, #country_container';
				break;
			case 'usersAndDevices':
				id = 'user_container';
				break;			
			case 'states':
				id = 'state_container';
				break;
			case 'age':
				id = 'age_container';
				break;
			case 'gender':
				id = 'gender_container';
				break;	
			case 'usersPerDay':
				id = 'total_session_chart_container';
				break;			
		}

		return id;
	}

	function buildNewReportByType(data, type){

		switch(type){
			case 'country':
				buildWorldMap(data);
				createCountryList();
				break;
			case 'usersAndDevices':
				animateUsersCount(data);
				buildDeviceChartAndList(data);
				break;			
			case 'states':
				createStatesList(data);
				break;
			case 'age':
				buildAgeChartAndList(data);
				break;
			case 'gender':
				buildGenderChartAndList(data);
				break;
			case 'usersPerDay':
				buildusersPerDayChart(data);
				break;
		}

		hideAmchartsLogo();
	}

	function hideAmchartsLogo(){
	
		$.each( $('g[filter*="url"]'), function(index, value){
			
			if( $(this).parent().is(':not(g[role="tooltip"])') ){
				$(this).css('opacity', '0.05').css('cursor', 'inherit');
			}

		});
	
	}

  	/** Loading Process */

	function initiateLoaders(){

		$('.chart-wrapper').append(`
			<div class = "smoke-screen" style = "display:none">
				<div class = "loader-wrapper">
					<div class="loader">Loading...</div>
				</div>
			</div>`
		);

		$('.smoke-screen').fadeIn();
	}

	function stopLoadingProcess(){

		$('.data-report').attr('disabled', false).removeClass('disabled');

		$('.smoke-screen').fadeOut(450, function(){
			$('.smoke-screen').remove();
		});
	}

	function addSmokeScreen(id){

		$(`#${id}`).append(`
			<div class = "smoke-screen" style = "display:none">
				<div class = "loader-wrapper">
					<div class="loader">Loading...</div>
				</div>
			</div>`
		);

		$(`#${id} .smoke-screen`).fadeIn();

	}

	function removeSmokeScreen(id){
		$(`#${id} .smoke-screen`).fadeOut(450, function(){
			$(`#${id} .smoke-screen`).remove();
		});
	}

	/** List Builder */

	function buildNewList(type, listId, list, unsetValue, newList = true){

		var data = [];
		var total = unsetValue;

		if( $(`#${listId} li`).length > 0 && newList){
			$(`#${listId} li`).remove();
		}

		$.each(list, function(index, value){

			var elementInfo = {};

			switch(type){
				case 'countryWithUsers':

					elementInfo.name =  value.name;
					elementInfo.percentage = formatPercentage(value.percentage);
					elementInfo.value = value.number;
					elementInfo.flag = `<?=HTTP_ROOT?>img/flags/${value.abbreviation.toLowerCase()}.png`;
					classPrefix = 'country';

					break;
				case 'countryWithoutUsers':
					
					if( $(`.country-name:contains('${value}')`).length === 0 ){
											
						elementInfo.name =  value;
						elementInfo.percentage = formatPercentage(0.00);
						elementInfo.value = 0;
						elementInfo.flag = `<?=HTTP_ROOT?>img/flags/${index.toLowerCase()}.png`;
						classPrefix = 'country';
					}

					break;
				case 'state':
					
					name = ( index == '(not set)' ) ? "<?= __(notSet) ?>" : index.replace('State of', '').trim();

					elementInfo.name =  name;
					elementInfo.value = value;
					elementInfo.percentage = calculatePercentage(elementInfo.value, total, true);
					classPrefix = 'state';

					break;
				case 'age':
					
					elementInfo.name =  value.name;
					elementInfo.value = value.value;
					elementInfo.percentage = calculatePercentage(elementInfo.value, total, true);
					classPrefix = 'age';

					break;	
				case 'gender':

					elementInfo.name =  value.name;
					elementInfo.value = value.value;
					elementInfo.percentage = calculatePercentage(elementInfo.value, total, true);
					classPrefix = 'gender';

					break;	
				case 'empty':
					elementInfo.name =  '';
					elementInfo.value = 0;
					elementInfo.percentage = calculatePercentage(elementInfo.value, total, true);
					classPrefix = 'empty';
				default:
					elementInfo.name =  value.name;
					elementInfo.value = value.value;
					elementInfo.percentage = calculatePercentage(elementInfo.value, total, true);

					classPrefix = 'default';
			}

			if( elementInfo.name ){
				unsetValue -= elementInfo.value;
				data.push(elementInfo);
			}

		});

		if(unsetValue > 0){
			
			$.each(data, function(index, value){

				if(value.value <= unsetValue){

					var unsetElement = {
						name: "<?= __(notSet) ?>",
						value: unsetValue,
						percentage: calculatePercentage(unsetValue, total, true),
					}

					data.splice(index, 0, unsetElement);
					unsetValue = -1;
				}

			});

		}
		
		$.each(data, function(index, value){
			value.ranking = $(`#${listId} li`).length + 1;
			addElementToList(listId, value, classPrefix);
		});
	
		return data;
	}

	function addElementToList(listId, elementInfo, classPrefix){

		var listElement = $(`
			<li> 
				<span class = "list-ranking ${classPrefix}-ranking"> ${elementInfo.ranking}. </span> 
				<a href = "#" > 
					<span class = "list-name ${classPrefix}-name">${elementInfo.name}</span> 
				</a> 
				<span class = "list-number ${classPrefix}-number"> ${elementInfo.value} <span class = "list-percentage ${classPrefix}-percentage"> ( ${elementInfo.percentage}% ) </span> </span> 
			</li>`);

		if(elementInfo.flag){
			$( listElement.find('a').get(0) ).prepend(`<span class = "list-flag ${classPrefix}-flag"> <img src = "${elementInfo.flag}" > </span>`)
		}

		$(`#${listId}`).append(listElement);
	}

	function calculatePercentage(value, total, format = false){

		var percentage = Math.round( ( (value/total) + Number.EPSILON) * 10000)/100;

		percentage = ( isNaN(percentage) ) ? 0 : percentage;

		if(format){
			return formatPercentage(percentage);
		}
		else{
			return percetange
		}
	}

	function formatPercentage(value){
		var percentage = parseFloat(value).toFixed(2)
		return ( percentage.toString().length === 4 ) ? '0'+percentage : percentage;
	}

	/** World Map */

	function buildWorldMap(reportData){

		countries = [];
		var data = [];

		if(worldMap && Object.keys(reportData).length > 1 ){

			$.each(reportData, function(index, value){

				if(index == 'total'){
					return false;
				}

				var abbreviation = ( country_abbreviations[ index ] ) ? country_abbreviations[ index ] : 'notset';
				var name = ( worldMapCountrysName[abbreviation] ) ? worldMapCountrysName[abbreviation] : "<?= __(notSet) ?>";

				countries.push( {name: name, abbreviation: abbreviation, number: value, percentage: Math.round( ( (value/totalUsers) + Number.EPSILON) * 10000)/100 } );

				data.push( { id: abbreviation, value: value } );
			});

			polygonSeries.data = data;
		}else{
			worldMap = am4core.create("world_chart", am4maps.MapChart);

			// Set map definition
			worldMap.geodata = am4geodata_worldLow;
			worldMap.geodataNames = worldMapCountrysName;

			// Set projection
			worldMap.projection = new am4maps.projections.Miller();

			worldMap.chartContainer.background.events.on("hit", function () { zoomOut() });

			// Create map polygon series
			polygonSeries = worldMap.series.push(new am4maps.MapPolygonSeries());

			// Make map load polygon (like country names) data from GeoJSON
			polygonSeries.useGeodata = true;

			// Configure series
			var polygonTemplate = polygonSeries.mapPolygons.template;
			polygonTemplate.tooltipText = "{name}: {value}";
			polygonTemplate.fill = am4core.color("#999");

			// Create hover state and set alternative fill color
			var hs = polygonTemplate.states.create("hover");
			hs.properties.fill = am4core.color("#367B25");

			// Remove Antarctica
			polygonSeries.exclude = ["AQ"];

			// polygonSeries.data = data;

			// Add heat rule
			polygonSeries.heatRules.push({
				"property": "fill",
				"target": polygonSeries.mapPolygons.template,
				"min": am4core.color("#6b848e"),
				"max": am4core.color("#006a92")
			});

			// Add heat legend
			var heatLegend = worldMap.createChild(am4maps.HeatLegend);
			heatLegend.series = polygonSeries;
			heatLegend.align = "right";
			heatLegend.width = am4core.percent(25);
			heatLegend.marginRight = 60;
			heatLegend.marginBottom = am4core.percent(3);
			heatLegend.valign = "bottom";

			var minRange = heatLegend.valueAxis.axisRanges.create();
			minRange.value = heatLegend.minValue;
			minRange.label.text = "0";
			// heatMaxRange = heatLegend.valueAxis.axisRanges.create();
			// heatMaxRange.value = heatLegend.maxValue;
			// heatMaxRange.label.text = heatMaxRange.value;


			heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
				return "";
			});

			polygonSeries.mapPolygons.template.events.on("over", function(ev) {

				if (!isNaN(ev.target.dataItem.value)) {
					heatLegend.valueAxis.showTooltipAt(ev.target.dataItem.value)
				}
				else {
					heatLegend.valueAxis.hideTooltip();
				}

				$(`#country_lista li:contains('${ev.target.dataItem.dataContext.name}')`).addClass('active');
			});

			polygonSeries.mapPolygons.template.events.on("out", function(ev) {
				heatLegend.valueAxis.hideTooltip();
				$('#country_lista li').removeClass('active');
			});

			// what to do when country is clicked
			polygonTemplate.events.on("hit", function (ev) {
				ev.target.zIndex = 1000000;

				if (!isNaN(ev.target.dataItem.value)) {
					console.log(ev);
				}

				// selectPolygon(ev.target);
			})

			function selectPolygon(ev){

				if (!isNaN(ev.target.dataItem.value)) {
					console.log(ev);
				}
				
			}

			//Zoom and Home Controls
			worldMap.zoomControl = new am4maps.ZoomControl();
			worldMap.smallMap = new am4maps.SmallMap();
			worldMap.smallMap.series.push(polygonSeries);

			let buttonHome = worldMap.chartContainer.createChild(am4core.Button);
			buttonHome.padding(5, 5, 5, 5);
			buttonHome.align = "right";
			buttonHome.marginRight = 5;
			buttonHome.tooltipY = 30;
			buttonHome.tooltipText = 'Home';
			buttonHome.events.on("hit", function() {
				worldMap.goHome();
			});

			buttonHome.icon = new am4core.Image();
			buttonHome.icon.href = '<?=HTTP_ROOT?>img/home.svg';
			buttonHome.icon.width = 15;
			buttonHome.icon.height = 15;

			// let buttonWorld = worldMap.chartContainer.createChild(am4core.Button);
			// buttonWorld.padding(5, 5, 5, 5);
			// buttonWorld.align = "right";
			// buttonWorld.marginRight = 15;
			// buttonWorld.tooltipText = 'All Countries';
			// buttonWorld.tooltipY = 25;
			// buttonWorld.events.on("hit", function() {
			// 	$('.data-report.active').click();
			// });

			// buttonWorld.icon = new am4core.Image();
			// buttonWorld.icon.href = '<?=HTTP_ROOT?>img/globe.svg';
			// buttonWorld.icon.width = 15;
			// buttonWorld.icon.height = 15;

		}

	}

	/** Country List */

	function createCountryList(){

		buildNewList('countryWithUsers', 'country_lista', countries, totalUsers);
		buildNewList('countryWithoutUsers', 'country_lista', worldMapCountrysName, 0, false);
		animateValue('country_total_number', totalUsers);

	}

	/** State List */

	function createStatesList(reportData){
		delete reportData['total'];

		if( totalUsers > 0 ){
			buildNewList('state', 'state_list', reportData, totalUsers);
		}
		else{
			buildNewList('state', 'state_list', brazilStates, 0);
		}

		animateValue('state_total_number', totalUsers);

	}

	/** Users Count */

	function animateValue(id, targetValue = 0, animationDuration = 1500, frameDuration = 1000){
			
		var frameDuration = frameDuration/60;
		var totalFrames = Math.round( animationDuration / frameDuration );
		var easeOutQuad = function(x){ return 1 - (1 - x) * (1 - x) };

		var frame = 0;

		var obj = document.getElementById(id);

		var animateInterval = setInterval(function(){
			frame++;

			var progress = easeOutQuad(frame/totalFrames);
			var currentCount = Math.round(targetValue * progress);

			if( parseInt( obj.innerHTML, 10 ) !== currentCount ){
				obj.innerHTML = formatNumber(currentCount);
			}

			if(frame == totalFrames){
				clearInterval(animateInterval);
			}

		},frameDuration);
	}

	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
	}

	function animateUsersCount(reportData){
		
		animateValue('new_user_total_number', reportData['new_users'] );
		animateValue('user_total_number', reportData['total_users'] );
		// animateValue('sessions_total_number', 0);
	
	}

	/** Device Chart/List */
	
	function buildDeviceChartAndList(reportData){

		if( deviceChart && Object.keys(reportData).length > 1 ){

			var mobileUsers = reportData['mobile_users'];
			var desktopUsers = reportData['desktop_users'];

			animateValue('mobile_number_wrapper', mobileUsers);
			animateValue('desktop_number_wrapper', desktopUsers);

			var data = [
				{
					Device: "Mobile",
					Users: mobileUsers,
				},
				{
					Device: "Desktop",
					Users: desktopUsers,
				},
			];

			reanimateChart(deviceChart, data);
		}
		else{

			animateValue('mobile_number_wrapper', 0);
			animateValue('desktop_number_wrapper', 0);

			am4core.ready(function() {

				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				deviceChart = am4core.create("device_chart", am4charts.PieChart);
				deviceChart.hiddenState.properties.opacity = 0; // this creates initial fade-in

				deviceChart.data = [{
					"Device": "Dummy",
					"disabled": true,
					"Users": 1000,
					"color": am4core.color("#dadada"),
					"opacity": 0.3,
					"strokeDasharray": "4,4",
					"tooltip": ""
				}];

				var series = deviceChart.series.push(new am4charts.PieSeries3D());
				series.dataFields.value = "Users";
				series.dataFields.category = "Device";

				series.labels.template.disabled = true;
				series.ticks.template.disabled = true;
				series.alignLabels = false;

				series.dataFields.hiddenInLegend = "disabled";
				series.labels.template.propertyFields.disabled = "disabled";
				series.ticks.template.propertyFields.disabled = "disabled";

				series.hiddenState.properties.opacity = 1;
				series.hiddenState.properties.endAngle = -90;
				series.hiddenState.properties.startAngle = -90;

				deviceChart.hiddenState.properties.radius = am4core.percent(0);

				/* Set up slice appearance */
				var slice = series.slices.template;
				slice.propertyFields.fill = "color";
				slice.propertyFields.fillOpacity = "opacity";
				slice.propertyFields.stroke = "color";
				slice.propertyFields.strokeDasharray = "strokeDasharray";
				slice.propertyFields.tooltipText = "tooltip";

			}); // end am4core.ready()
		
		}

	}

	/** Age Chart/List */

	function buildAgeChartAndList(reportData){

		var unsetAgeUsers = totalUsers;
		var data = [];

		if(Object.keys(reportData).length > 0){

			$.each(reportData, function(index, value){

				if(index == 'total'){
					return false;
				}

				var ageBracket = index;
				var ageUsers = value;

				unsetAgeUsers -= parseInt(ageUsers);

				delete ageBracketsAux[ageBracket];
				data.push({name: ageBracket, value: ageUsers});
			});

		}

		$.each(ageBracketsAux, function(index, value){
			data.push({name: index, value: '0'});
		});

		data = buildNewList('age', 'age_list', data, totalUsers);

		if(ageChart && totalUsers > 0){
			reanimateChart(ageChart, data);
		}
		else{

			am4core.ready(function() {

				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				ageChart = am4core.create("age_chart", am4charts.PieChart);
				ageChart.hiddenState.properties.opacity = 0; // this creates initial fade-in

				ageChart.data = [{
					"name": "Dummy",
					"disabled": true,
					"value": 1000,
					"color": am4core.color("#dadada"),
					"opacity": 0.3,
					"strokeDasharray": "4,4",
					"tooltip": ""
				}];

				var series = ageChart.series.push(new am4charts.PieSeries3D());
				series.dataFields.value = "value";
				series.dataFields.category = "name";

				series.labels.template.disabled = true;
				series.ticks.template.disabled = true;
				series.alignLabels = false;

				series.dataFields.hiddenInLegend = "disabled";
				series.labels.template.propertyFields.disabled = "disabled";
				series.ticks.template.propertyFields.disabled = "disabled";

				/* Set up slice appearance */
				var slice = series.slices.template;
				slice.propertyFields.fill = "color";
				slice.propertyFields.fillOpacity = "opacity";
				slice.propertyFields.stroke = "color";
				slice.propertyFields.strokeDasharray = "strokeDasharray";
				slice.propertyFields.tooltipText = "tooltip";

			}); // end am4core.ready()
		}

		ageBracketsAux = Object.assign({}, ageBrackets);
	}

	/** Gender Chart/List */

	function buildGenderChartAndList(reportData){

		var unsetGenderUsers = totalUsers;
		var data = [];

		if(Object.keys(reportData).length > 0){

			$.each(reportData, function(index, value){

				if(index == 'total'){
					return false;
				}

				var genderName = index;
				var genderUsers = value;

				unsetGenderUsers -= parseInt(genderUsers);

				delete gendersAux[genderName];

				name = (genderName == 'male') ? "<?= __(male) ?>" : "<?= __(female) ?>" 

				data.push({name: name, value: genderUsers});
			});

		}

		$.each(gendersAux, function(index, value){
			name = (index == 'male') ? "<?= __(male) ?>" : "<?= __(female) ?>" 
			data.push({name: name, value: '0'});
		});

		data = buildNewList('gender', 'gender_list', data, totalUsers);

		if(genderChart && totalUsers > 0 ){
			reanimateChart(genderChart, data);
		}
		else{

			am4core.ready(function() {

				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				genderChart = am4core.create("gender_chart", am4charts.PieChart);
				genderChart.hiddenState.properties.opacity = 0; // this creates initial fade-in

				genderChart.data = [{
					"name": "Dummy",
					"disabled": true,
					"value": 1000,
					"color": am4core.color("#dadada"),
					"opacity": 0.3,
					"strokeDasharray": "4,4",
					"tooltip": ""
				}];

				var series = genderChart.series.push(new am4charts.PieSeries3D());
				series.dataFields.value = "value";
				series.dataFields.category = "name";

				series.labels.template.disabled = true;
				series.ticks.template.disabled = true;
				series.alignLabels = false;

				series.dataFields.hiddenInLegend = "disabled";
				series.labels.template.propertyFields.disabled = "disabled";
				series.ticks.template.propertyFields.disabled = "disabled";

				/* Set up slice appearance */
				var slice = series.slices.template;
				slice.propertyFields.fill = "color";
				slice.propertyFields.fillOpacity = "opacity";
				slice.propertyFields.stroke = "color";
				slice.propertyFields.strokeDasharray = "strokeDasharray";
				slice.propertyFields.tooltipText = "tooltip";

			}); // end am4core.ready()

		}

		gendersAux = Object.assign({}, genders);
	}

	/** Users Chart */

	function buildusersPerDayChart(reportData){
	
		var data = [];
		var averageValue = 0;

		if(Object.keys(reportData).length > 1){

			$.each(reportData, function(index, value){

				if(index == 'total'){
					return false;
				}

				var date = index.slice(0,4) + '-' + index.slice(4,6) + '-' + index.slice(6);

				data.push({date: date, users: value})

			});

			averageValue = (reportData['total']/(Object.keys(reportData).length - 1) );

		}
	
		if(usersChart){
			usersAverageLine.value = averageValue;
			reanimateChart(usersChart, data);
		}
		else{

			am4core.ready(function() {

				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				usersChart = am4core.create("total_session_chart", am4charts.XYChart);
				usersChart.hiddenState.properties.opacity = 0; // this creates initial fade-in
				usersChart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

				usersChart.data = [{}];

				// Create axes
				var dateAxis = usersChart.xAxes.push(new am4charts.DateAxis());
				dateAxis.renderer.grid.template.location = 0;
				dateAxis.renderer.minGridDistance = 50;

				var valueAxis = usersChart.yAxes.push(new am4charts.ValueAxis());
				valueAxis.min = 0;
				valueAxis.renderer.minGridDistance = 20;

				// Create series
				var series = usersChart.series.push(new am4charts.LineSeries());
				series.dataFields.valueY = "users";
				series.dataFields.dateX = "date";
				series.tooltipText = "{value}"
				series.strokeWidth = 2;
				series.minBulletDistance = 15;

				series.tooltip.background.cornerRadius = 20;
				series.tooltip.background.strokeOpacity = 0;
				series.tooltip.pointerOrientation = "vertical";
				series.tooltip.label.minWidth = 40;
				series.tooltip.label.minHeight = 40;
				series.tooltip.label.textAlign = "middle";
				series.tooltip.label.textValign = "middle";

				var bullet = series.bullets.push(new am4charts.CircleBullet());
				bullet.circle.strokeWidth = 2;
				bullet.circle.radius = 4;
				bullet.circle.fill = am4core.color("#fff");

				var bullethover = bullet.states.create("hover");
				bullethover.properties.scale = 1.3;

				usersChart.cursor = new am4charts.XYCursor();
				usersChart.cursor.behavior = "panXY";
				usersChart.cursor.xAxis = dateAxis;
				usersChart.cursor.snapToSeries = series;

				usersChart.cursor.fullWidthLineX = true;
				usersChart.cursor.lineX.strokeWidth = 0;
				usersChart.cursor.lineX.fill = am4core.color("#000");
				usersChart.cursor.lineX.fillOpacity = 0.1;

				// Create vertical scrollbar and place it before the value axis
				usersChart.scrollbarY = new am4core.Scrollbar();
				usersChart.scrollbarY.parent = usersChart.leftAxesContainer;
				usersChart.scrollbarY.toBack();

				// Create a horizontal scrollbar with previe and place it underneath the date axis
				usersChart.scrollbarX = new am4charts.XYChartScrollbar();
				usersChart.scrollbarX.series.push(series);
				usersChart.scrollbarX.parent = usersChart.bottomAxesContainer;

				// dateAxis.start = 0.79;
				dateAxis.keepSelection = true;

				// Add a guide
				usersAverageLine = valueAxis.axisRanges.create();
				usersAverageLine.value = averageValue;
				usersAverageLine.grid.stroke = am4core.color("#036b92");
				usersAverageLine.grid.strokeWidth = 1;
				usersAverageLine.grid.strokeOpacity = 1;
				usersAverageLine.grid.strokeDasharray = "3,3";
				usersAverageLine.label.inside = true;
				usersAverageLine.label.text = "<?= __(average) ?>";
				usersAverageLine.label.fill = usersAverageLine.grid.stroke;
				usersAverageLine.label.verticalCenter = "bottom";

			}); // end am4core.ready()

		}

	}

	function reanimateChart(chart, data){

		chart.hide();

		chart.data = data;
		
		setTimeout(function(){
			chart.appear();
		}, 500);

	}

</script>

<!-- Load the JavaScript API client and Sign-in library. -->

<style>

	h1{
		text-align: center;
	}

	h1, h2, h3, h4, h5, h6{
		font-family: 'Lato', sans-serif;
	}

	h3{
		font-weight: bold;
	}

	#user_total_number, #new_user_total_number, #mobile_number_wrapper, #desktop_number_wrapper, #country_total_number, #state_total_number{
		font-weight: 300;
	}

	.page-content{
		margin-top: -20px;
	}

	.amChart-logo{
		opacity: 0.05;
	}

	.page-content{
		background: #f3f3f3;
	}

	#charts_wrapper{
		width: 100%;
		padding: 1em;
		margin: auto;
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: space-around;
	}

	/** Buttons Row */

	.buttons-row{
		flex-basis: 100%;
		padding: 1.5em;
		margin: 1em;
		display: flex;
		flex-direction: row;
		flex-wrap: nowrap;
		justify-content: flex-end;
		align-items: center;
		box-shadow: 0px 0px 7px 0px #0000001f;
		background-color: white;
		border-radius: 5px !important;
	}

	#header_wrapper{
		flex-grow: 1;
	}

	#header_wrapper h1{
		margin: 0;
		color: #5daaa7;
		font-weight: bold;
		text-align: left;
	}

	.buttons-wrapper{
		flex-basis: 5%;
		text-align: right;
	}

	.data-report{
		width: 70px;
		height: 30px;
		margin-left: 10px;
		border: 1px solid #5daaa7;
		border-radius: 20px !important;
		color: #5daaa7;
		transition: ease 0.5s all;
		cursor: pointer;	
		font-weight: bolder;
		background-color: white;
	}

	.data-report:hover{
		transform: scale(1.15);
		background-color: #5daaa7;
		color: white;
	}

	.data-report:focus{
		outline: none;
	}

	.data-report.disabled{
		opacity: 0.4;
	}

	.data-report.active{
		background-color: #5daaa7;
		color: white;
	}

	/** Charts */

	.chart-wrapper{
		flex-grow: 1;
		flex-basis: 48%;
		flex-wrap: wrap;
		margin: 1em;
		min-width: 400px;
		height: 500px;
		box-shadow: 0px 0px 7px 0px #0000001f;
		background-color: white;
    	border-radius: 5px !important;
		text-align: center;
		position: relative;
		overflow: hidden;
	}

	.chart-wrapper a{
		cursor: default;
	}

	.chart-wrapper.triple-column{
		flex-basis: 21%;
	}

	.chart{
		height: 100%;
	}

	#world_chart_container{
		flex-basis: 70%;
	}

	#world_chart_container h3{
		z-index: 1;
		position: absolute;
		top: 0;
		right: 50%;
		transform: translate(50%);
	}

	#user_container{
		max-width: 300px;
   		min-width: 300px;
		flex-basis: 100%;
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: space-around;
	}

	#user_container > div{
		flex-basis: 100%;
	}

	#device_number_container{
		float: left;
		margin-top: 20px;
	}

	#device_number_container h3{
		margin-left: 10px;
	}

	#device_chart{
	    float: right;
		height: 250px;
		width: 180px;	
	}

	#total_session_chart_container{
		flex-basis: 70%;
		padding-bottom: 40px;
	}

	/* #median_session_chart, #total_session_chart{
		width: 99%;
		margin: auto;
		height: 500px;
		margin-top: 0px;
	} */

	/** Country/State/Age/Gender Section */

	#country_container, #state_container, #age_container, #gender{
		overflow: auto;
	}

	#age_container, #gender_container{
		max-width: 300px;
		min-width: 300px;
		overflow: hidden;
	}

	.triple-column::-webkit-scrollbar-track{
		margin: 0;
		border-radius: 5px;
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	.triple-column::-webkit-scrollbar{
		width: 7px;
		background-color: #F5F5F5;
	}

	.triple-column::-webkit-scrollbar-thumb{
		border-radius: 5px;
		background-color: #337ab7;
	}
	
	.triple-column{
		overflow: auto;
	}

	#country_container > h3, #state_container > h3{
		float: left;
		margin-left: 10px;
	}

	#country_container > #country_total_number, #state_container > #state_total_number{
		float: right;
		margin-right: 10px;
	}

	.analytic-list-wrapper{
		clear: both;
	}

	.analytic-list{
		list-style: none;
		padding-left: 0;
		margin-bottom: 0;
	}

	.analytic-list li{
		height: 40px;
		border-top: 1px solid #e6e2e2;
		clear: both;
    	overflow: hidden;
	}

	#country_lista li.active{
		background-color: #fcc946;
	}

	#country_lista li.active span{
		background-color: #fcc946;
	}

	.list-flag, .list-name,{
		font-weight: bolder;
	}

	.list-ranking{
		float: left;
		padding-top: 10px;
		margin: 0 10px;
		width: 25px;
	}

	.country-flag, .list-name{
		float: left;
		margin-left: 10px;
		padding-top: 10px;
	}

	.country-flag img{
		height: 20px;
		width: 30px;
		box-shadow: 0px 0px 3px black;
    	border-radius: 2px !important;
	}

	.list-name{
		text-align: left;
		overflow: hidden;
		max-height: 30px;
		max-width: 45%;
	}

	.list-number{
		float: right;
		padding-right: 10px;
		padding-top: 10px;
		border-left: 1px solid #e6e2e2;
		border-bottom: 1px solid #e6e2e2;
		width: 30%;
		height: 40px;
		min-width: 90px;
		text-align: right;
		background-color: #fbfbfb;

	}

	.age-number, .gender-number{
		width: 40%;
	}

	.list-percentage{
		margin-left: 5px;
		opacity: 0.45;
	}

	#gender_chart{
		height: 320px;
	}

	/** Loader */

	.smoke-screen{
	    position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		z-index: 10;
		background-color: #cecece8c;	
		border-radius: 5px !important;
	}

	.loader-wrapper{		
		position: absolute;
		top: 50%;
		bottom: 0;
		left: 0;
		right: 0;
		transform: translate(0,-50%);
	}

	.loader, .loader:before, .loader:after {
		background: #fcc946;
		-webkit-animation: load1 1s infinite ease-in-out;
		animation: load1 1s infinite ease-in-out;
		width: 1em;
		height: 4em;
	}

	.loader {
		color: #fcc946;
		text-indent: -9999em;
		margin: 88px auto;
		position: relative;
		font-size: 11px;
		-webkit-transform: translateZ(0);
		-ms-transform: translateZ(0);
		transform: translateZ(0);
		-webkit-animation-delay: -0.16s;
		animation-delay: -0.16s;
	}

	.loader:before, .loader:after {
		position: absolute;
		top: 0;
		content: '';
	}

	.loader:before {
		left: -1.5em;
		-webkit-animation-delay: -0.32s;
		animation-delay: -0.32s;
	}
	
	.loader:after {
		left: 1.5em;
	}

	@-webkit-keyframes load1 {
		0%, 80%, 100% {
			box-shadow: 0 0;
			height: 4em;
		}
		40% {
			box-shadow: 0 -2em;
			height: 5em;
		}
	}

	@keyframes load1 {
		0%, 80%, 100% {
			box-shadow: 0 0;
			height: 4em;
		}
		40% {
			box-shadow: 0 -2em;
			height: 5em;
		}
	}

	@media(max-width: 1500px){

		#user_container{
			max-width: 100%;
			height: auto;
		}

		#user_container > div{
			flex-basis: 35%;
		}

		#user_container > div:nth-of-type(1) hr:nth-of-type(2){
			display: none;
		}

		#user_container > div:nth-of-type(2){
			flex-grow: 1;
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: flex-start;		
		}

		#device_number_container {
			float: none;
			margin-top: 0px;
			flex-basis: 50%;
			border-left: 1px solid #f0f0f0;
		}

		#device_number_container h3{
			margin-left: 0;
		}

		#device_chart{
			float: none;
			flex-basis: 50%;
			border-left: 1px solid #f0f0f0;
		}

		#total_session_chart_container, #world_chart_container, #user_container{
			flex-basis: 100%;
		}

		#country_container, #state_container{
			min-width: 390px;
		}

	}

	@media(max-width: 1250px){

		.buttons-row{
			flex-wrap: wrap;
		}

		#header_wrapper{
			flex-basis: 100%;
			margin-bottom: 15px;
		}

		#header_wrapper h1{
			text-align: center;
		}

	}    

	@media(max-width: 1050px){

		#country_container, #state_container, #total_session_chart_container, #world_chart_container, #user_container, #age_container, #gender_container{
			min-width: 100%;
			flex-basis: 100%;
		}

	}

	@media(max-width: 500px){
		
		.buttons-row{
		    justify-content: space-around;	
		}

		.data-report{
			margin: 0;
			padding: 0;
			width: 60px;
		}

		.chart-wrapper, #age_container, #gender_container{
			flex-basis: 100%;
			max-width: 100%;
			min-width: 100%;
		}

		#user_container{
			display: block;
			height: 500px;
		}

		#user_container > div:nth-of-type(1) hr:nth-of-type(2){
			display: block;
		}

		#user_container > div:nth-of-type(2){
			display: block;
		}

		#device_number_container{
			float: left;
			border-left: none;
			margin-top: 15px;
		}

		#device_number_container h3{
			margin-left: 10px;
		}

		#device_chart{
			float: right;
			flex-basis: 50%;
			border-left: none;
		}

	}

</style>